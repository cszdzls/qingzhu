$(function(){
	//进入页面时检查购物车中是否有商品
	if( $(".outer9 ul li").length === 0){
		$(" .outer9 .inter3").hide();
		$(" .outer9 .inter22").hide();
		$(" .outer9 .inter23").show();
	}
});

//全选
$(".checkAll").click(function () {
	if($(this).prop("checked")){
		$(":checkbox").prop("checked",true);
	}
	else{
		$(":checkbox").prop("checked",false);
	}
	lTotal();
});
//单选
$(".check").click(function () {
	// console.log($(".outer9 ul :checked").length);
	// console.log($(".outer9 ul li").length);
	if($(".outer9 ul :checked").length === $(".outer9 ul li").length){
		$(".checkAll").prop("checked",true);
	}
	else{
		$(".checkAll").prop("checked",false);
	}
	lTotal();
});

//购物车项数量加
function add(e,id) {
	var domId = 'goodsCount' + id;
	var goodsCount = parseInt($("#" + domId).text());
	$("#" + domId).text(goodsCount+1);
	if (updateItem(id)===-1){
		$("#" + domId).text(goodsCount);
	}
	total(e);
	lTotal();
}

//购物车项数量减
function sub(e,id) {
	var domId = 'goodsCount' + id;
	var goodsCount = parseInt($("#" + domId).text());
	$("#" + domId).text(goodsCount-1);
	if (updateItem(id)===-1){
		$("#" + domId).text(goodsCount);
	}
	total(e);
	lTotal();
}

/**
 *更新购物项
 *
 * todo 判断是否与原值相同
 */
function updateItem(id) {
	var domId = 'goodsCount' + id;
	var goodsCount = parseInt($("#" + domId).text());
	if (goodsCount > 5) {
		swal("单个商品最多可购买5个", {
			icon: "error",
		});
		return -1;
	}
	if (goodsCount < 1) {
		swal("数量异常", {
			icon: "error",
		});
		return -1;
	}
	var data = {
		"id": id,
		"goodsCount": goodsCount
	};
	$.ajax({
		type: 'PUT',
		url: '/shop-cart',
		contentType: 'application/json',
		data: JSON.stringify(data),
		success: function (result) {
			if (result.resultCode === 200) {
				// window.location.reload();
			} else {
				swal("操作失败", {
					icon: "error",
				});
				return -1;
			}
		},
		error: function () {
			swal("操作失败", {
				icon: "error",
			});
			return -1;
		}
	});
}
//小计
function total(ele) {
	var price = parseFloat($(ele).siblings(".price").html());
	var count = parseFloat($(ele).siblings(".count").html());
	var total = price * count;
	$(ele).siblings(".total").html(total);
}
//总计
function lTotal() {
	var totalCount = 0;
	var totalPrice = 0;
	$("ul :checked").each(function () {
		totalCount += parseFloat($(this).siblings(".count").html());
		totalPrice += parseFloat($(this).siblings(".total").html());
	});
	$('.totalCount').html(totalCount);
	$('.totalPrice').html('￥'+totalPrice);
}
//删除
// $(".outer9 .del").click(function () {
// 	if (!confirm("确认删除购物车记录吗？")){
// 		return;
// 	}
// 	goodsNo = $(this).attr("goodsno");
// 	that = this;
// 	$.ajax({
// 		type:"delete",
// 		url:"/shopcart/delete/goods_no/"+goodsNo,
// 		dataType:"json",
// 		data:{},
// 		success:function(result){
// 			// window.location.href=result.data.url;
// 			$(that).parents("li").remove();
// 			if( $(".outer9 ul li").length === 0){
// 				$(".outer9 .inter3").hide();
// 				$(".outer9 .inter22").hide();
// 				$(".outer9 .checkAll").prop('checked',false);
// 				$(".outer9 .inter23").show();
// 			}
// 			alert("删除成功");
// 		},
// 		error:function(XMLHttpRequest, textStatus){
// 			alert("异常: "+XMLHttpRequest.status+" "+textStatus);
// 		}
// 	});
// });
$(".outer9 .ss1").click(function () {
	($("ul :checked")).parents("li").remove();
	if( $(".outer9 ul li").length === 0){
		$(" .outer9 .inter3").hide();
		$(" .outer9 .inter22").hide();
		$(" .outer9 .inter23").show();
	}
});

//==========================================================================
