package com.zq.qingzhu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.zq.qingzhu.dao")
@SpringBootApplication
public class QingzhuApplication {
	public static void main(String[] args) {
		SpringApplication.run(QingzhuApplication.class, args);
	}
}
