package com.zq.qingzhu.bean;

/**
 * 库存修改所需实体
 */
public class StockNumDTO {
	private Integer goodsId;

	private Integer goodsCount;

	public Integer getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}

	public Integer getGoodsCount() {
		return goodsCount;
	}

	public void setGoodsCount(Integer goodsCount) {
		this.goodsCount = goodsCount;
	}
}
