package com.zq.qingzhu.interceptor;

import com.zq.qingzhu.bean.GoodsDetail;
import com.zq.qingzhu.bean.ShopCart;
import com.zq.qingzhu.bean.ShopCartExample;
import com.zq.qingzhu.common.Constants;
import com.zq.qingzhu.controller.vo.UserVO;
import com.zq.qingzhu.dao.GoodsDetailMapper;
import com.zq.qingzhu.dao.ShopCartMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 购物车数量处理
 */
@Component
public class CartNumberInterceptor implements HandlerInterceptor {

	@Autowired
	private ShopCartMapper shopCartMapper;
	@Autowired
	private GoodsDetailMapper goodsDetailMapper;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
		//购物车中的数量会更改，但是在这些接口中并没有对session中的数据做修改，这里统一处理一下
		if (null != request.getSession() && null != request.getSession().getAttribute(Constants.MALL_USER_SESSION_KEY)) {
			//如果当前为登陆状态，就查询数据库并设置购物车中的数量值
			UserVO userVO = (UserVO) request.getSession().getAttribute(Constants.MALL_USER_SESSION_KEY);
			//设置购物车中的数量和总价
			int count = 0;
			BigDecimal totalPrice = new BigDecimal("0.00");
			ShopCartExample example = new ShopCartExample();
			example.createCriteria().andUserIdEqualTo(userVO.getUserId());
			List<ShopCart> shopCartList = shopCartMapper.selectByExample(example);
			//用户购物车中没有数据，无需计算总数和总价
			if (shopCartList.size()==0){
				userVO.setShopCartCount(count);
				userVO.setShopCartTotalMoney(totalPrice);
				request.getSession().setAttribute(Constants.MALL_USER_SESSION_KEY, userVO);
				return true;
			}
			List<Integer> goodsIdList = new ArrayList<>();
			for (ShopCart shopCart:shopCartList){
				goodsIdList.add(shopCart.getGoodsId());
			}
//			List<GoodsDetail> goodsDetailList = goodsDetailMapper.selectByPrimaryKeys(goodsIdList);
//			//如果goodsIdList只有一个元素，使用selectByPrimaryKeys时SQL会报错
//			if (goodsIdList.size()==1){
//				GoodsDetail goodsDetail = goodsDetailMapper.selectByPrimaryKey(goodsIdList.get(0));
//				goodsDetailList.add(goodsDetail);
//			}else{
//				goodsDetailList = goodsDetailMapper.selectByPrimaryKeys(goodsIdList);
//			}
			List<GoodsDetail> goodsDetailList = goodsDetailMapper.selectByPrimaryKeys(goodsIdList);
			for (int i=0;i<goodsDetailList.size();i++){
				totalPrice = totalPrice.add(goodsDetailList.get(i).getSellingPrice()
						.multiply(BigDecimal.valueOf(shopCartList.get(i).getGoodsCount())));
			}
			count = shopCartList.size();
			userVO.setShopCartCount(count);
			userVO.setShopCartTotalMoney(totalPrice);
			request.getSession().setAttribute(Constants.MALL_USER_SESSION_KEY, userVO);
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

	}
}
