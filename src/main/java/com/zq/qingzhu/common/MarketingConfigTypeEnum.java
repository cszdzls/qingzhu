package com.zq.qingzhu.common;

/**
 * @apiNote 营销类型 1-新品推荐，精心挑选 2-专题活动，限时促销 3-青竹良品，你的家居首选 4-全球大牌优选，买手用心挑选 5-良品体验
 */
public enum MarketingConfigTypeEnum {

	DEFAULT(0, "DEFAULT"),
	MARKETING_NEW(1, "MARKETING_NEW"),
	MARKETING_SALE(2, "MARKETING_SALE"),
	MARKETING_GREAT(3, "MARKETING_GREAT"),
	MARKETING_BRAND(4, "MARKETING_BRAND"),
	MARKETING_EXPERIENCE(5, "MARKETING_EXPERIENCE");

	private int type;

	private String name;

	MarketingConfigTypeEnum(int type, String name) {
		this.type = type;
		this.name = name;
	}

	public static MarketingConfigTypeEnum getMarketingConfigTypeEnumByType(int type) {
		for (MarketingConfigTypeEnum marketingConfigTypeEnum : MarketingConfigTypeEnum.values()) {
			if (marketingConfigTypeEnum.getType() == type) {
				return marketingConfigTypeEnum;
			}
		}
		return DEFAULT;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
