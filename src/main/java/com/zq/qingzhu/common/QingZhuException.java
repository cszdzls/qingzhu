package com.zq.qingzhu.common;

public class QingZhuException extends RuntimeException {

	public QingZhuException() {
	}

	public QingZhuException(String message) {
		super(message);
	}

	/**
	 * 丢出一个异常
	 *
	 * @param message
	 */
	public static void fail(String message) {
		throw new QingZhuException(message);
	}

}
