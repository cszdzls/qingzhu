package com.zq.qingzhu.common;

/**
 * @apiNote 常量配置
 */
public class Constants {
	//public final static String FILE_UPLOAD_DIC = "/opt/image/upload/";//上传文件的默认url前缀，根据部署设置自行修改
	public final static String FILE_UPLOAD_DIC = "D:\\upload\\";//上传文件的默认url前缀，根据部署设置自行修改

	public final static int INDEX_CAROUSEL_NUMBER = 3;//首页轮播图数量

	public final static int INDEX_CATEGORY_NUMBER = 10;//首页一级分类的最大数量

	public final static int SEARCH_CATEGORY_NUMBER = 8;//搜索页一级分类的最大数量

	public final static int MARKETING_NEW_NUMBER = 4;//首页新品推荐，精心挑选数量
	public final static int MARKETING_SALE_NUMBER = 3;//首页专题活动，限时促销数量
	public final static int MARKETING_GREAT_NUMBER = 8;//首页青竹良品，你的家居首选数量
	public final static int MARKETING_BRAND_NUMBER = 4;//首页全球大牌优选，买手用心挑选数量
	public final static int MARKETING_EXPERIENCE_NUMBER = 4;//首页良品体验数量

	public final static int SHOPPING_CART_ITEM_TOTAL_NUMBER = 13;//购物车中商品的最大数量(可根据自身需求修改)

	public final static int SHOPPING_CART_ITEM_LIMIT_NUMBER = 5;//购物车中单个商品的最大购买数量(可根据自身需求修改)

	public final static String MALL_VERIFY_CODE_KEY = "mallVerifyCode";//验证码key

	public final static String MALL_USER_SESSION_KEY = "mallUser";//session中user的key

	public final static int GOODS_SEARCH_PAGE_LIMIT = 10;//搜索分页的默认条数(每页10条)

	public final static int ALL_PRODUCTS_PAGE_MAX_LIMIT = 12;//所有商品页面每页显示的商品最大条数

	public final static int ORDER_SEARCH_PAGE_LIMIT = 5;//我的订单列表分页的默认条数

	public final static int SELL_STATUS_UP = 0;//商品上架状态
	public final static int SELL_STATUS_DOWN = 1;//商品下架状态

}
