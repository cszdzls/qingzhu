package com.zq.qingzhu.controller.mall;

import com.zq.qingzhu.bean.ShopCart;
import com.zq.qingzhu.common.Constants;
import com.zq.qingzhu.common.ServiceResultEnum;
import com.zq.qingzhu.controller.vo.ShoppingCartItemVO;
import com.zq.qingzhu.controller.vo.UserVO;
import com.zq.qingzhu.service.ShopCartService;
import com.zq.qingzhu.util.Result;
import com.zq.qingzhu.util.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;

@Controller
public class ShopCartController {

	@Resource
	private ShopCartService shopCartService;

	/**
	 * 进入购物车页面
	 *
	 * @param request     HttpServletRequest
	 * @param httpSession HttpSession
	 * @return qingzhu/front/shopcart
	 */
	@GetMapping({"/shop-cart","/shopcart.html"})
	public String cartListPage(HttpServletRequest request,
							   HttpSession httpSession) {
		UserVO user = (UserVO) httpSession.getAttribute(Constants.MALL_USER_SESSION_KEY);
		int itemsTotal = 0;
		BigDecimal priceTotal = new BigDecimal("0.00");
		List<ShoppingCartItemVO> myShopCarts = shopCartService.getMyShoppingCartItems(user.getUserId());
		if (!CollectionUtils.isEmpty(myShopCarts)) {
			//订单项总数
			itemsTotal = myShopCarts.stream().mapToInt(ShoppingCartItemVO::getGoodsCount).sum();
//			if (itemsTotal < 1) {
//				return "error/error_5xx";
//			}
			//总价
			for (ShoppingCartItemVO shoppingCartItemVO : myShopCarts) {
				priceTotal = priceTotal.add(shoppingCartItemVO.getSellingPrice().multiply(BigDecimal.valueOf(shoppingCartItemVO.getGoodsCount())));
			}
			if (priceTotal.intValue() < 1) {
				return "error/error_5xx";
			}
		}
		request.setAttribute("itemsTotal", itemsTotal);
		request.setAttribute("priceTotal", priceTotal);
		request.setAttribute("myShoppingCartItems", myShopCarts);
//		return "mall/cart";
		return "qingzhu/front/shopcart";
	}

	/**
	 * 添加购物车项
	 *
	 * @param shopCart		ShopCart
	 * @param httpSession	HttpSession
	 * @return 200, SUCCESS或500, FAIL
	 */
	@PostMapping("/shop-cart")
	@ResponseBody
	public Result saveShopCart(@RequestBody ShopCart shopCart,
							   HttpSession httpSession) {
		UserVO user = (UserVO) httpSession.getAttribute(Constants.MALL_USER_SESSION_KEY);
		shopCart.setUserId(user.getUserId());
		//todo 判断数量
		String saveResult = shopCartService.saveShopCart(shopCart);
		//添加成功
		if (ServiceResultEnum.SUCCESS.getResult().equals(saveResult)) {
			return ResultGenerator.genSuccessResult();
		}
		//添加失败
		return ResultGenerator.genFailResult(saveResult);
	}

	/**
	 * 修改购物车商品数量
	 *
	 * @param shopCart ShopCart
	 * @param httpSession      HttpSession
	 * @return 200, SUCCESS或500, FAIL
	 */
	@PutMapping("/shop-cart")
	@ResponseBody
	public Result updateShopCart(@RequestBody ShopCart shopCart,
								 HttpSession httpSession) {
		UserVO user = (UserVO) httpSession.getAttribute(Constants.MALL_USER_SESSION_KEY);
		shopCart.setUserId(user.getUserId());
		//todo 判断数量
		String saveResult = shopCartService.updateShopCart(shopCart);
		//修改成功
		if (ServiceResultEnum.SUCCESS.getResult().equals(saveResult)) {
			return ResultGenerator.genSuccessResult();
		}
		//修改失败
		return ResultGenerator.genFailResult(saveResult);
	}

	/**
	 * 删除购物车项
	 *
	 * @param ShopCartId		Integer
	 * @param httpSession		HttpSession
	 * @return 200, SUCCESS或500, FAIL
	 */
	@DeleteMapping("/shop-cart/{ShopCartId}")
	@ResponseBody
	public Result updateShopCart(@PathVariable("ShopCartId") Integer ShopCartId,
								 HttpSession httpSession) {
		UserVO user = (UserVO) httpSession.getAttribute(Constants.MALL_USER_SESSION_KEY);
		Boolean deleteResult = shopCartService.deleteById(ShopCartId);
		//删除成功
		if (deleteResult) {
			return ResultGenerator.genSuccessResult();
		}
		//删除失败
		return ResultGenerator.genFailResult(ServiceResultEnum.OPERATE_ERROR.getResult());
	}

	/**
	 * 跳转到结算页面
	 *
	 * @param request     HttpServletRequest
	 * @param httpSession HttpSession
	 * @return 支付页面
	 */
	@GetMapping("/shop-cart/settle")
	public String settlePage(HttpServletRequest request,
							 HttpSession httpSession) {
		BigDecimal priceTotal = new BigDecimal("0.00");
		UserVO user = (UserVO) httpSession.getAttribute(Constants.MALL_USER_SESSION_KEY);
		List<ShoppingCartItemVO> myShopCarts = shopCartService.getMyShoppingCartItems(user.getUserId());
		if (CollectionUtils.isEmpty(myShopCarts)) {
			//无数据则不跳转至结算页
			return "/shop-cart";
		} else {
			//总价
			for (ShoppingCartItemVO shoppingCartItemVO : myShopCarts) {
				priceTotal = priceTotal.add(shoppingCartItemVO.getSellingPrice().multiply(BigDecimal.valueOf(shoppingCartItemVO.getGoodsCount())));
			}
//			if (priceTotal.intValue() < 1) {
//				return "error/error_5xx";
//			}
		}
		request.setAttribute("priceTotal", priceTotal);
		request.setAttribute("myShoppingCartItems", myShopCarts);
		return "qingzhu/front/order.html";
	}
}
