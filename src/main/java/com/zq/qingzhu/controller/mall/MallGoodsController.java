package com.zq.qingzhu.controller.mall;

import com.zq.qingzhu.bean.GoodsDetail;
import com.zq.qingzhu.common.Constants;
import com.zq.qingzhu.controller.vo.GoodsDetailVO;
import com.zq.qingzhu.service.CategoryService;
import com.zq.qingzhu.service.GoodsDetailService;
import com.zq.qingzhu.util.BeanUtil;
import com.zq.qingzhu.util.PageQueryUtil;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class MallGoodsController {

	@Resource
	private GoodsDetailService goodsDetailService;
	@Resource
	private CategoryService categoryService;

//	@GetMapping({"/search", "/search.html"})
//	public String searchPage(@RequestParam Map<String, Object> params, HttpServletRequest request) {
//		if (StringUtils.isEmpty(params.get("page"))) {
//			params.put("page", 1);
//		}
//		params.put("limit", Constants.GOODS_SEARCH_PAGE_LIMIT);
//		//封装分类数据
//		if (params.containsKey("goodsCategoryId") && !StringUtils.isEmpty(params.get("goodsCategoryId") + "")) {
//			Integer categoryId = Integer.valueOf(params.get("goodsCategoryId") + "");
//			SearchPageCategoryVO searchPageCategoryVO = categoryService.getCategoriesForSearch(categoryId);
//			if (searchPageCategoryVO != null) {
//				request.setAttribute("goodsCategoryId", categoryId);
//				request.setAttribute("searchPageCategoryVO", searchPageCategoryVO);
//			}
//		}
//		//封装参数供前端回显
//		if (params.containsKey("orderBy") && !StringUtils.isEmpty(params.get("orderBy") + "")) {
//			request.setAttribute("orderBy", params.get("orderBy") + "");
//		}
//		String keyword = "";
//		//对keyword做过滤 去掉空格
//		if (params.containsKey("keyword") && !StringUtils.isEmpty((params.get("keyword") + "").trim())) {
//			keyword = params.get("keyword") + "";
//		}
//		request.setAttribute("keyword", keyword);
//		params.put("keyword", keyword);
//		//封装商品数据
//		PageQueryUtil pageUtil = new PageQueryUtil(params);
//		request.setAttribute("pageResult", goodsDetailService.searchGoods(pageUtil));
//		return "mall/search";
//	}

//	@GetMapping("/search")
//	public String searchPage(@RequestParam Map<String, Object> params, @PathVariable String brandName, HttpServletRequest request) {
//		//如无页数参数则默认显示第一页
//		if (StringUtils.isEmpty(params.get("page"))) {
//			params.put("page", 1);
//		}
//		//如无每页显示条数或每页显示条数大于最大值或每页显示条数小于0，则默认显示ALL_PRODUCTS_PAGE_MAX_LIMIT条每页
//		if (StringUtils.isEmpty(params.get("limit")) ||
//				Integer.parseInt(params.get("limit").toString())>Constants.ALL_PRODUCTS_PAGE_MAX_LIMIT ||
//				Integer.parseInt(params.get("limit").toString())<0){
//			params.put("limit", Constants.ALL_PRODUCTS_PAGE_MAX_LIMIT);
//		}
//		//如无品牌筛选参数则默认不筛选
//		if (StringUtils.isEmpty(params.get("brand"))) {
//			params.put("brand", null);
//		}
//		//如无商品类别(id)筛选参数则默认不筛选
//		if (StringUtils.isEmpty(params.get("goodsCategoryId"))) {
//			params.put("goodsCategoryId", null);
//		}
//		//封装商品数据
//		PageQueryUtil pageUtil = new PageQueryUtil(params);
//		request.setAttribute("pageResult", goodsDetailService.searchGoods(pageUtil));
//		return "qingzhu/front/allproducts";
//	}

	@GetMapping({"/allproducts", "/allproducts.html"})
	public String allProducts(@RequestParam(required = false) Map<String, Object> params, HttpServletRequest request) {
		//如无页数参数则默认显示第一页
		if (StringUtils.isEmpty(params.get("page"))) {
			params.put("page", 1);
		}
		//如无每页显示条数或每页显示条数大于最大值或每页显示条数小于0，则默认显示ALL_PRODUCTS_PAGE_MAX_LIMIT条每页
		if (StringUtils.isEmpty(params.get("limit")) ||
		Integer.parseInt(params.get("limit").toString())>Constants.ALL_PRODUCTS_PAGE_MAX_LIMIT ||
		Integer.parseInt(params.get("limit").toString())<0){
			params.put("limit", Constants.ALL_PRODUCTS_PAGE_MAX_LIMIT);
		}

		//如无品牌筛选参数则默认不筛选
		if (StringUtils.isEmpty(params.get("brand"))) {
			params.put("brand", null);
		}else{
			request.setAttribute("brandChecked", params.get("brand").toString());
		}
		//如无商品类别(id)筛选参数则默认不筛选
		if (StringUtils.isEmpty(params.get("category"))) {
			params.put("category", null);
		}else{
			Integer categoryId = categoryService.getIdByCategoryName(params.get("category").toString());
			params.put("goodsCategoryId", categoryId);
			request.setAttribute("categoryChecked", params.get("category"));
		}
		//封装商品数据
		PageQueryUtil pageUtil = new PageQueryUtil(params);
		request.setAttribute("pageResult", goodsDetailService.searchGoods(pageUtil));
		//显示所有商品的品牌
		request.setAttribute("brandList", goodsDetailService.getBrandList());
		//显示所有商品类别
		request.setAttribute("categoryList", categoryService.getAllCategory());
//		//显示商品总数
//		request.setAttribute("goodsCount", goodsDetailService.getGoodsCount());
		return "qingzhu/front/allproducts";
	}

	@GetMapping("/goods/detail/{goodsId}")
	public String detailPage(@PathVariable("goodsId") Integer goodsId, HttpServletRequest request) {
		if (goodsId < 1) {
			return "error/error_5xx";
		}
		GoodsDetail goods = goodsDetailService.getGoodsById(goodsId);
		if (goods == null) {
			return "error/error_404";
		}
		GoodsDetailVO goodsDetailVO = new GoodsDetailVO();
		BeanUtil.copyProperties(goods, goodsDetailVO);
		goodsDetailVO.setGoodsId(goods.getId());
		goodsDetailVO.setGoodsDescription(goods.getDescription());
//		goodsDetailVO.setGoodsCarouselList(goods.getGoodsCarousel().split(","));
		request.setAttribute("goodsDetail", goodsDetailVO);
		return "qingzhu/front/detail";
	}

}
