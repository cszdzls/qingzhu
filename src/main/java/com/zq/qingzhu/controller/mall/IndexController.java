package com.zq.qingzhu.controller.mall;

import com.zq.qingzhu.common.Constants;
import com.zq.qingzhu.common.MarketingConfigTypeEnum;
import com.zq.qingzhu.controller.vo.IndexCarouselVO;
import com.zq.qingzhu.controller.vo.IndexCategoryVO;
import com.zq.qingzhu.controller.vo.IndexConfigGoodsVO;
import com.zq.qingzhu.service.CarouselService;
import com.zq.qingzhu.service.CategoryService;
import com.zq.qingzhu.service.MarketingConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class IndexController {

	@Resource
	private CarouselService carouselService;
	@Resource
	private MarketingConfigService marketingConfigService;
	@Resource
	private CategoryService categoryService;

	@GetMapping({"/index", "/", "/index.html"})
	public String indexPage(HttpServletRequest request) {
		List<IndexCategoryVO> categories = categoryService.getCategoriesForIndex();
		if (CollectionUtils.isEmpty(categories)) {//商品分类数据为空
			return "error/error_5xx";
		}
		List<IndexCarouselVO> carousels = carouselService.getCarouselsForIndex(Constants.INDEX_CAROUSEL_NUMBER);
		//新品推荐，精心挑选
		List<IndexConfigGoodsVO> marketingNew = marketingConfigService
				.getConfigGoodsesForIndex(MarketingConfigTypeEnum.MARKETING_NEW.getType(),
						Constants.MARKETING_NEW_NUMBER);
		//专题活动，限时促销
		List<IndexConfigGoodsVO> marketingSale = marketingConfigService
				.getConfigGoodsesForIndex(MarketingConfigTypeEnum.MARKETING_SALE.getType(),
						Constants.MARKETING_SALE_NUMBER);
		//青竹良品，你的家居首选
		List<IndexConfigGoodsVO> marketingGreat = marketingConfigService
				.getConfigGoodsesForIndex(MarketingConfigTypeEnum.MARKETING_GREAT.getType(),
						Constants.MARKETING_GREAT_NUMBER);
		//全球大牌优选，买手用心挑选
		List<IndexConfigGoodsVO> marketingBrand = marketingConfigService
				.getConfigGoodsesForIndex(MarketingConfigTypeEnum.MARKETING_BRAND.getType(),
						Constants.MARKETING_BRAND_NUMBER);
		//良品体验
		List<IndexConfigGoodsVO> marketingExperience = marketingConfigService
				.getConfigGoodsesForIndex(MarketingConfigTypeEnum.MARKETING_EXPERIENCE.getType(),
						Constants.MARKETING_EXPERIENCE_NUMBER);

		request.setAttribute("categories", categories);//分类数据
		request.setAttribute("carousels", carousels);//轮播图
		request.setAttribute("marketingNew", marketingNew);//新品推荐，精心挑选
		request.setAttribute("marketingSale", marketingSale);//专题活动，限时促销
		request.setAttribute("marketingGreat", marketingGreat);//青竹良品，你的家居首选
		request.setAttribute("marketingBrand", marketingBrand);//全球大牌优选，买手用心挑选
		request.setAttribute("marketingExperience", marketingExperience);//良品体验
		return "qingzhu/front/index";
	}
}
