package com.zq.qingzhu.controller.mall;

import com.zq.qingzhu.bean.User;
import com.zq.qingzhu.common.Constants;
import com.zq.qingzhu.common.ServiceResultEnum;
import com.zq.qingzhu.controller.vo.UserVO;
import com.zq.qingzhu.service.OrderService;
import com.zq.qingzhu.service.ShopCartService;
import com.zq.qingzhu.service.UserService;
import com.zq.qingzhu.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class PersonalController {

	@Resource
	private UserService userService;
	@Autowired
	private ShopCartService shopCartService;
	@Autowired
	private OrderService orderService;

	@GetMapping({"/personal","/userinfo.html"})
	public String personalPage(HttpServletRequest request,
							   HttpSession httpSession) {
		request.setAttribute("path", "personal");
//		return "mall/personal";
		return "qingzhu/front/userinfo";
	}

	@GetMapping("/logout")
	public String logout(HttpSession httpSession) {
		httpSession.removeAttribute(Constants.MALL_USER_SESSION_KEY);
//		return "mall/login";
		return "qingzhu/front/login";
	}

	@GetMapping({"/login", "/login.html"})
	public String loginPage() {
//		return "mall/login";
		return "qingzhu/front/login";
	}

	@GetMapping({"/register", "/register.html"})
	public String registerPage() {
//		return "mall/register";
		return "qingzhu/front/register";
	}

	@GetMapping("/personal/addresses")
	public String addressesPage() {
		return "mall/addresses";
	}

	@PostMapping("/login")
	@ResponseBody
	public Result login(@RequestParam("loginName") String loginName,
						@RequestParam("password") String password,
						HttpSession httpSession) {
		if (StringUtils.isEmpty(loginName)) {
			return ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_NAME_NULL.getResult());
		}
		if (StringUtils.isEmpty(password)) {
			return ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_PASSWORD_NULL.getResult());
		}
		//todo 清verifyCode
		String loginResult = userService.login(loginName, SHA512.getSHA512(password), httpSession);
		//登录成功
		if (ServiceResultEnum.SUCCESS.getResult().equals(loginResult)) {
			return ResultGenerator.genSuccessResult();
		}
		//登录失败
		return ResultGenerator.genFailResult(loginResult);
	}

	@PostMapping("/register")
	@ResponseBody
	public Result register(@RequestParam("loginName") String loginName,
						   @RequestParam("password") String password,
						   HttpSession httpSession) {
		if (StringUtils.isEmpty(loginName)) {
			return ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_NAME_NULL.getResult());
		}
		if (StringUtils.isEmpty(password)) {
			return ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_PASSWORD_NULL.getResult());
		}
		//todo 清verifyCode
		String registerResult = userService.register(loginName, password);
		//注册成功
		if (ServiceResultEnum.SUCCESS.getResult().equals(registerResult)) {
			return ResultGenerator.genSuccessResult();
		}
		//注册失败
		return ResultGenerator.genFailResult(registerResult);
	}

	@PostMapping("/personal/updateInfo")
	@ResponseBody
	public Result updateInfo(@RequestBody User user, HttpSession httpSession) {
		UserVO userTemp = userService.updateUserInfo(user, httpSession);
		if (userTemp == null) {
			return ResultGenerator.genFailResult("修改失败");
		} else {
			//返回成功
			return ResultGenerator.genSuccessResult();
		}
	}

	@GetMapping("/personal/orders")
	public String ordersPage(@RequestParam Map<String, Object> params,
							 HttpServletRequest request){
		//如无页数参数则默认第一页
		if (StringUtils.isEmpty(params.get("page"))){
			params.put("page", 1);
		}
		//如无每页显示订单个数参数则默认每页Constants.ORDER_SEARCH_PAGE_LIMIT个
		if (StringUtils.isEmpty(params.get("limit"))){
			params.put("limit", Constants.ORDER_SEARCH_PAGE_LIMIT);
		}
		//获得userId
		if (request.getSession().getAttribute("mallUser")==null){
			return "qingzhu/front/login";
		}
		params.put("userId",((UserVO)request.getSession().getAttribute("mallUser")).getUserId());
		PageQueryUtil pageQueryUtil = new PageQueryUtil(params);
		PageResult myOrders = orderService.getMyOrders(pageQueryUtil);
		request.setAttribute("myOrders", myOrders);
		return "qingzhu/front/userorders";
	}
}
