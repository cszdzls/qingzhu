package com.zq.qingzhu.controller.admin;

import com.zq.qingzhu.bean.Category;
import com.zq.qingzhu.common.ServiceResultEnum;
import com.zq.qingzhu.service.CategoryService;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.Result;
import com.zq.qingzhu.util.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping("/admin")
public class CategoryController {

	@Resource
	private CategoryService categoryService;

	@GetMapping("/categories")
	public String categoriesPage(HttpServletRequest request, @RequestParam("categoryLevel") Byte categoryLevel, @RequestParam("parentId") Long parentId, @RequestParam("backParentId") Long backParentId) {
		if (categoryLevel == null || categoryLevel < 1 || categoryLevel > 3) {
			return "error/error_5xx";
		}
		request.setAttribute("path", "newbee_mall_category");
		request.setAttribute("parentId", parentId);
		request.setAttribute("backParentId", backParentId);
		request.setAttribute("categoryLevel", categoryLevel);
		return "admin/newbee_mall_category";
	}

	/**
	 * 列表
	 */
	@RequestMapping(value = "/categories/list", method = RequestMethod.GET)
	@ResponseBody
	public Result list(@RequestParam Map<String, Object> params) {
		if (StringUtils.isEmpty(params.get("page")) || StringUtils.isEmpty(params.get("limit"))) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		PageQueryUtil pageUtil = new PageQueryUtil(params);
		return ResultGenerator.genSuccessResult(categoryService.getCategorisPage(pageUtil));
	}

	/**
	 * 列表
	 */
	@RequestMapping(value = "/categories/listForSelect", method = RequestMethod.GET)
	@ResponseBody
	public Result listForSelect(@RequestParam("categoryId") Integer categoryId) {
//		if (categoryId == null || categoryId < 1) {
//			return ResultGenerator.genFailResult("缺少参数！");
//		}
//		Category category = categoryService.getGoodsCategoryById(categoryId);
//		//既不是一级分类也不是二级分类则为不返回数据
//		if (category == null || category.getCategoryLevel() == CategoryLevelEnum.LEVEL_THREE.getLevel()) {
//			return ResultGenerator.genFailResult("参数异常！");
//		}
//		Map categoryResult = new HashMap(2);
//		if (category.getCategoryLevel() == CategoryLevelEnum.LEVEL_ONE.getLevel()) {
//			//如果是一级分类则返回当前一级分类下的所有二级分类，以及二级分类列表中第一条数据下的所有三级分类列表
//			//查询一级分类列表中第一个实体的所有二级分类
//			List<Category> secondLevelCategories = categoryService.selectByLevelAndParentIdsAndNumber(Collections.singletonList(categoryId), CategoryLevelEnum.LEVEL_TWO.getLevel());
//			if (!CollectionUtils.isEmpty(secondLevelCategories)) {
//				//查询二级分类列表中第一个实体的所有三级分类
//				List<Category> thirdLevelCategories = categoryService.selectByLevelAndParentIdsAndNumber(Collections.singletonList(secondLevelCategories.get(0).getCategoryId()), CategoryLevelEnum.LEVEL_THREE.getLevel());
//				categoryResult.put("secondLevelCategories", secondLevelCategories);
//				categoryResult.put("thirdLevelCategories", thirdLevelCategories);
//			}
//		}
//		if (category.getCategoryLevel() == CategoryLevelEnum.LEVEL_TWO.getLevel()) {
//			//如果是二级分类则返回当前分类下的所有三级分类列表
//			List<Category> thirdLevelCategories = categoryService.selectByLevelAndParentIdsAndNumber(Collections.singletonList(categoryId), CategoryLevelEnum.LEVEL_THREE.getLevel());
//			categoryResult.put("thirdLevelCategories", thirdLevelCategories);
//		}
//		return ResultGenerator.genSuccessResult(categoryResult);
		return null;
	}

	/**
	 * 添加
	 */
	@RequestMapping(value = "/categories/save", method = RequestMethod.POST)
	@ResponseBody
	public Result save(@RequestBody Category goodsCategory) {
		if (Objects.isNull(goodsCategory.getCategoryName())) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		String result = categoryService.saveCategory(goodsCategory);
		if (ServiceResultEnum.SUCCESS.getResult().equals(result)) {
		return ResultGenerator.genSuccessResult();
		} else {
			return ResultGenerator.genFailResult(result);
		}
	}


	/**
	 * 修改
	 */
	@RequestMapping(value = "/categories/update", method = RequestMethod.POST)
	@ResponseBody
	public Result update(@RequestBody Category goodsCategory) {
//		if (Objects.isNull(goodsCategory.getId())
//				|| Objects.isNull(goodsCategory.getCategoryLevel())
//				|| StringUtils.isEmpty(goodsCategory.getCategoryName())
//				|| Objects.isNull(goodsCategory.getParentId())
//				|| Objects.isNull(goodsCategory.getCategoryRank())) {
//			return ResultGenerator.genFailResult("参数异常！");
//		}
//		String result = categoryService.updateGoodsCategory(goodsCategory);
//		if (ServiceResultEnum.SUCCESS.getResult().equals(result)) {
//			return ResultGenerator.genSuccessResult();
//		} else {
//			return ResultGenerator.genFailResult(result);
//		}
		return null;
	}

	/**
	 * 详情
	 */
	@GetMapping("/categories/info/{id}")
	@ResponseBody
	public Result info(@PathVariable("id") Integer id) {
		Category goodsCategory = categoryService.getCategoryById(id);
		if (goodsCategory == null) {
			return ResultGenerator.genFailResult("未查询到数据");
		}
		return ResultGenerator.genSuccessResult(goodsCategory);
	}

	/**
	 * 分类删除
	 */
	@RequestMapping(value = "/categories/delete", method = RequestMethod.POST)
	@ResponseBody
	public Result delete(@RequestBody Integer[] ids) {
		if (ids.length < 1) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		if (categoryService.deleteBatch(ids)) {
			return ResultGenerator.genSuccessResult();
		} else {
			return ResultGenerator.genFailResult("删除失败");
		}
	}


}