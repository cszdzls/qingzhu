package com.zq.qingzhu.controller.admin;

import com.zq.qingzhu.bean.OrderRecord;
import com.zq.qingzhu.common.ServiceResultEnum;
import com.zq.qingzhu.controller.vo.OrderItemVO;
import com.zq.qingzhu.service.OrderService;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.Result;
import com.zq.qingzhu.util.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping("/admin")
public class AdminOrderController {

	@Resource
	private OrderService orderService;

	@GetMapping("/orders")
	public String ordersPage(HttpServletRequest request) {
		request.setAttribute("path", "orders");
		return "admin/newbee_mall_order";
	}

	/**
	 * 列表
	 */
	@RequestMapping(value = "/orders/list", method = RequestMethod.GET)
	@ResponseBody
	public Result list(@RequestParam Map<String, Object> params) {
		if (StringUtils.isEmpty(params.get("page")) || StringUtils.isEmpty(params.get("limit"))) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		PageQueryUtil pageUtil = new PageQueryUtil(params);
		return ResultGenerator.genSuccessResult(orderService.getOrdersPage(pageUtil));
	}

	/**
	 * 修改
	 */
	@RequestMapping(value = "/orders/update", method = RequestMethod.POST)
	@ResponseBody
	public Result update(@RequestBody OrderRecord orderRecord) {
		if (Objects.isNull(orderRecord.getTotalPrice())
				|| Objects.isNull(orderRecord.getId())
				|| orderRecord.getId() < 1
				|| orderRecord.getTotalPrice().intValue() < 1
				|| StringUtils.isEmpty(orderRecord.getUserAddress())) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		String result = orderService.updateOrderInfo(orderRecord);
		if (ServiceResultEnum.SUCCESS.getResult().equals(result)) {
			return ResultGenerator.genSuccessResult();
		} else {
			return ResultGenerator.genFailResult(result);
		}
	}

	/**
	 * 详情
	 */
	@GetMapping("/order-items/{id}")
	@ResponseBody
	public Result info(@PathVariable("id") Integer id) {
		List<OrderItemVO> orderItems = orderService.getOrderItems(id);
		if (!CollectionUtils.isEmpty(orderItems)) {
			return ResultGenerator.genSuccessResult(orderItems);
		}
		return ResultGenerator.genFailResult(ServiceResultEnum.DATA_NOT_EXIST.getResult());
	}

	/**
	 * 配货
	 */
	@RequestMapping(value = "/orders/checkDone", method = RequestMethod.POST)
	@ResponseBody
	public Result checkDone(@RequestBody Integer[] ids) {
		if (ids.length < 1) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		String result = orderService.checkDone(ids);
		if (ServiceResultEnum.SUCCESS.getResult().equals(result)) {
			return ResultGenerator.genSuccessResult();
		} else {
			return ResultGenerator.genFailResult(result);
		}
	}

	/**
	 * 出库
	 */
	@RequestMapping(value = "/orders/checkOut", method = RequestMethod.POST)
	@ResponseBody
	public Result checkOut(@RequestBody Integer[] ids) {
		if (ids.length < 1) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		String result = orderService.checkOut(ids);
		if (ServiceResultEnum.SUCCESS.getResult().equals(result)) {
			return ResultGenerator.genSuccessResult();
		} else {
			return ResultGenerator.genFailResult(result);
		}
	}

	/**
	 * 关闭订单
	 */
	@RequestMapping(value = "/orders/close", method = RequestMethod.POST)
	@ResponseBody
	public Result closeOrder(@RequestBody Integer[] ids) {
		if (ids.length < 1) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		String result = orderService.closeOrder(ids);
		if (ServiceResultEnum.SUCCESS.getResult().equals(result)) {
			return ResultGenerator.genSuccessResult();
		} else {
			return ResultGenerator.genFailResult(result);
		}
	}

}