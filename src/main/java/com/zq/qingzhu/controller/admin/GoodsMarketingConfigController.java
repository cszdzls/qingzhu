package com.zq.qingzhu.controller.admin;

import com.zq.qingzhu.bean.MarketingConfig;
import com.zq.qingzhu.common.MarketingConfigTypeEnum;
import com.zq.qingzhu.common.ServiceResultEnum;
import com.zq.qingzhu.service.MarketingConfigService;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.Result;
import com.zq.qingzhu.util.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping("/admin")
public class GoodsMarketingConfigController {

	@Resource
	private MarketingConfigService marketingConfigService;

	@GetMapping("/indexConfigs")
	public String indexConfigsPage(HttpServletRequest request, @RequestParam("configType") int configType) {
		MarketingConfigTypeEnum marketingConfigTypeEnum = MarketingConfigTypeEnum.getMarketingConfigTypeEnumByType(configType);
		if (marketingConfigTypeEnum.equals(MarketingConfigTypeEnum.DEFAULT)) {
			return "error/error_5xx";
		}

		request.setAttribute("path", marketingConfigTypeEnum.getName());
		request.setAttribute("configType", configType);
		return "admin/newbee_mall_index_config";
	}

	/**
	 * 列表
	 */
	@RequestMapping(value = "/indexConfigs/list", method = RequestMethod.GET)
	@ResponseBody
	public Result list(@RequestParam Map<String, Object> params) {
		if (StringUtils.isEmpty(params.get("page")) || StringUtils.isEmpty(params.get("limit"))) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		PageQueryUtil pageUtil = new PageQueryUtil(params);
		return ResultGenerator.genSuccessResult(marketingConfigService.getConfigsPage(pageUtil));
	}

	/**
	 * 添加
	 */
	@RequestMapping(value = "/indexConfigs/save", method = RequestMethod.POST)
	@ResponseBody
	public Result save(@RequestBody MarketingConfig marketingConfig) {
		if (Objects.isNull(marketingConfig.getMarketingType())
//				|| StringUtils.isEmpty(marketingConfig.getConfigName())
				|| Objects.isNull(marketingConfig.getRank())) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		String result = marketingConfigService.saveMarketingConfig(marketingConfig);
		if (ServiceResultEnum.SUCCESS.getResult().equals(result)) {
			return ResultGenerator.genSuccessResult();
		} else {
			return ResultGenerator.genFailResult(result);
		}
	}


	/**
	 * 修改
	 */
	@RequestMapping(value = "/indexConfigs/update", method = RequestMethod.POST)
	@ResponseBody
	public Result update(@RequestBody MarketingConfig marketingConfig) {
		if (Objects.isNull(marketingConfig.getMarketingType())
//				|| Objects.isNull(marketingConfig.getConfigId())
//				|| StringUtils.isEmpty(marketingConfig.getConfigName())
				|| Objects.isNull(marketingConfig.getRank())) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		String result = marketingConfigService.updateMarketingConfig(marketingConfig);
		if (ServiceResultEnum.SUCCESS.getResult().equals(result)) {
			return ResultGenerator.genSuccessResult();
		} else {
			return ResultGenerator.genFailResult(result);
		}
	}

	/**
	 * 详情
	 */
	@GetMapping("/indexConfigs/info/{id}")
	@ResponseBody
	public Result info(@PathVariable("id") Integer id) {
		MarketingConfig config = marketingConfigService.getMarketingConfigById(id);
		if (config == null) {
			return ResultGenerator.genFailResult("未查询到数据");
		}
		return ResultGenerator.genSuccessResult(config);
	}

	/**
	 * 删除
	 */
	@RequestMapping(value = "/indexConfigs/delete", method = RequestMethod.POST)
	@ResponseBody
	public Result delete(@RequestBody Integer[] ids) {
		if (ids.length < 1) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		if (marketingConfigService.deleteBatch(ids)) {
			return ResultGenerator.genSuccessResult();
		} else {
			return ResultGenerator.genFailResult("删除失败");
		}
	}


}