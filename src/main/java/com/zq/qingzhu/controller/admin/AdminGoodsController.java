package com.zq.qingzhu.controller.admin;

import com.zq.qingzhu.bean.Category;
import com.zq.qingzhu.bean.GoodsDetail;
import com.zq.qingzhu.common.Constants;
import com.zq.qingzhu.common.ServiceResultEnum;
import com.zq.qingzhu.service.CategoryService;
import com.zq.qingzhu.service.GoodsDetailService;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.Result;
import com.zq.qingzhu.util.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping("/admin")
public class AdminGoodsController {

	@Resource
	private GoodsDetailService goodsDetailService;
	@Resource
	private CategoryService categoryService;

	@GetMapping("/goods")
	public String goodsPage(HttpServletRequest request) {
		request.setAttribute("path", "newbee_mall_goods");
		return "admin/newbee_mall_goods";
	}

	@GetMapping("/goods/edit")
	public String edit(HttpServletRequest request) {
		request.setAttribute("path", "edit");

		List<Category> categories = categoryService.getAllCategory();
				request.setAttribute("categories", categories);
				request.setAttribute("path", "goods-edit");
				return "admin/newbee_mall_goods_edit";
	}

	@GetMapping("/goods/edit/{goodsId}")
	public String edit(HttpServletRequest request, @PathVariable("goodsId") Integer goodsId) {
//		request.setAttribute("path", "edit");
//		GoodsDetail goodsDetail = goodsDetailService.getGoodsById(goodsId);
//		if (goodsDetail == null) {
//			return "error/error_400";
//		}
//		if (goodsDetail.getGoodsCategoryId() > 0) {
//			if (goodsDetail.getGoodsCategoryId() != null || goodsDetail.getGoodsCategoryId() > 0) {
//				//有分类字段则查询相关分类数据返回给前端以供分类的三级联动显示
//				Category currentGoodsCategory = categoryService.getCategoryById(goodsDetail.getGoodsCategoryId());
//				//商品表中存储的分类id字段为三级分类的id，不为三级分类则是错误数据
//				if (currentGoodsCategory != null && currentGoodsCategory.getCategoryLevel() == CategoryLevelEnum.LEVEL_THREE.getLevel()) {
//					//查询所有的一级分类
//					List<Category> firstLevelCategories = categoryService.selectByLevelAndParentIdsAndNumber(Collections.singletonList(0L), CategoryLevelEnum.LEVEL_ONE.getLevel());
//					//根据parentId查询当前parentId下所有的三级分类
//					List<Category> thirdLevelCategories = categoryService.selectByLevelAndParentIdsAndNumber(Collections.singletonList(currentGoodsCategory.getParentId()), CategoryLevelEnum.LEVEL_THREE.getLevel());
//					//查询当前三级分类的父级二级分类
//					Category secondCategory = categoryService.getGoodsCategoryById(currentGoodsCategory.getParentId());
//					if (secondCategory != null) {
//						//根据parentId查询当前parentId下所有的二级分类
//						List<Category> secondLevelCategories = categoryService.selectByLevelAndParentIdsAndNumber(Collections.singletonList(secondCategory.getParentId()), CategoryLevelEnum.LEVEL_TWO.getLevel());
//						//查询当前二级分类的父级一级分类
//						Category firestCategory = categoryService.getGoodsCategoryById(secondCategory.getParentId());
//						if (firestCategory != null) {
//							//所有分类数据都得到之后放到request对象中供前端读取
//							request.setAttribute("firstLevelCategories", firstLevelCategories);
//							request.setAttribute("secondLevelCategories", secondLevelCategories);
//							request.setAttribute("thirdLevelCategories", thirdLevelCategories);
//							request.setAttribute("firstLevelCategoryId", firestCategory.getCategoryId());
//							request.setAttribute("secondLevelCategoryId", secondCategory.getCategoryId());
//							request.setAttribute("thirdLevelCategoryId", currentGoodsCategory.getCategoryId());
//						}
//					}
//				}
//			}
//		}
//		if (goodsDetail.getGoodsCategoryId() == 0) {
//			//查询所有的一级分类
//			List<Category> firstLevelCategories = categoryService.selectByLevelAndParentIdsAndNumber(Collections.singletonList(0L), CategoryLevelEnum.LEVEL_ONE.getLevel());
//			if (!CollectionUtils.isEmpty(firstLevelCategories)) {
//				//查询一级分类列表中第一个实体的所有二级分类
//				List<Category> secondLevelCategories = categoryService.selectByLevelAndParentIdsAndNumber(Collections.singletonList(firstLevelCategories.get(0).getCategoryId()), CategoryLevelEnum.LEVEL_TWO.getLevel());
//				if (!CollectionUtils.isEmpty(secondLevelCategories)) {
//					//查询二级分类列表中第一个实体的所有三级分类
//					List<Category> thirdLevelCategories = categoryService.selectByLevelAndParentIdsAndNumber(Collections.singletonList(secondLevelCategories.get(0).getCategoryId()), CategoryLevelEnum.LEVEL_THREE.getLevel());
//					request.setAttribute("firstLevelCategories", firstLevelCategories);
//					request.setAttribute("secondLevelCategories", secondLevelCategories);
//					request.setAttribute("thirdLevelCategories", thirdLevelCategories);
//				}
//			}
//		}
//		request.setAttribute("goods", goodsDetail);
//		request.setAttribute("path", "goods-edit");
		return "admin/newbee_mall_goods_edit";
	}

	/**
	 * 列表
	 */
	@RequestMapping(value = "/goods/list", method = RequestMethod.GET)
	@ResponseBody
	public Result list(@RequestParam Map<String, Object> params) {
		if (StringUtils.isEmpty(params.get("page")) || StringUtils.isEmpty(params.get("limit"))) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		PageQueryUtil pageUtil = new PageQueryUtil(params);
		return ResultGenerator.genSuccessResult(goodsDetailService.getGoodsPage(pageUtil));
	}

	/**
	 * 添加
	 */
	@RequestMapping(value = "/goods/save", method = RequestMethod.POST)
	@ResponseBody
	public Result save(@RequestBody GoodsDetail goods) {
		//goods.setGoodsCategoryId(1);
		if (StringUtils.isEmpty(goods.getGoodsName())
				|| StringUtils.isEmpty(goods.getDescription())
				|| Objects.isNull(goods.getGoodsCategoryId())
				|| Objects.isNull(goods.getSellingPrice())
				|| Objects.isNull(goods.getStock())
				|| Objects.isNull(goods.getGoodsSellStatus())
				|| StringUtils.isEmpty(goods.getGoodsCoverImg())) {
			System.out.println(goods.getGoodsName()+goods.getDescription()+goods.getGoodsCategoryId()+goods.getSellingPrice()+goods.getStock()+goods.getGoodsSellStatus()+goods.getGoodsCoverImg()+"这是测试数据！！！！！");
			return ResultGenerator.genFailResult("参数异常！");
		}
		String result = goodsDetailService.saveGoods(goods);
	if (ServiceResultEnum.SUCCESS.getResult().equals(result)) {
			return ResultGenerator.genSuccessResult();
		} else {
			return ResultGenerator.genFailResult(result);
		}
	}


	/**
	 * 修改
	 */
	@RequestMapping(value = "/goods/update", method = RequestMethod.POST)
	@ResponseBody
	public Result update(@RequestBody GoodsDetail goodsDetail) {
//		if (Objects.isNull(goods.getGoodsId())
//				|| StringUtils.isEmpty(goods.getGoodsName())
//				|| StringUtils.isEmpty(goods.getGoodsIntro())
//				|| StringUtils.isEmpty(goods.getTag())
//				|| Objects.isNull(goods.getOriginalPrice())
//				|| Objects.isNull(goods.getSellingPrice())
//				|| Objects.isNull(goods.getGoodsCategoryId())
//				|| Objects.isNull(goods.getStockNum())
//				|| Objects.isNull(goods.getGoodsSellStatus())
//				|| StringUtils.isEmpty(goods.getGoodsCoverImg())
//				|| StringUtils.isEmpty(goods.getGoodsDetailContent())) {
//			return ResultGenerator.genFailResult("参数异常！");
//		}
//		String result = goodsDetailService.updateNewBeeMallGoods(goods);
//		if (ServiceResultEnum.SUCCESS.getResult().equals(result)) {
//			return ResultGenerator.genSuccessResult();
//		} else {
//			return ResultGenerator.genFailResult(result);
//		}
		return null;
	}

	/**
	 * 详情
	 */
	@GetMapping("/goods/info/{id}")
	@ResponseBody
	public Result info(@PathVariable("id") Integer id) {
		GoodsDetail goodsDetail = goodsDetailService.getGoodsById(id);
		if (goodsDetail == null) {
			return ResultGenerator.genFailResult(ServiceResultEnum.DATA_NOT_EXIST.getResult());
		}
		return ResultGenerator.genSuccessResult(goodsDetail);
	}

	/**
	 * 批量修改销售状态
	 */
	@RequestMapping(value = "/goods/status/{sellStatus}", method = RequestMethod.PUT)
	@ResponseBody
	public Result delete(@RequestBody Integer[] ids, @PathVariable("sellStatus") int sellStatus) {
		if (ids.length < 1) {
			return ResultGenerator.genFailResult("参数异常！");
		}
		if (sellStatus != Constants.SELL_STATUS_UP && sellStatus != Constants.SELL_STATUS_DOWN) {
			return ResultGenerator.genFailResult("状态异常！");
		}
		if (goodsDetailService.batchUpdateSellStatus(ids, sellStatus)) {
			return ResultGenerator.genSuccessResult();
		} else {
			return ResultGenerator.genFailResult("修改失败");
		}
	}

}