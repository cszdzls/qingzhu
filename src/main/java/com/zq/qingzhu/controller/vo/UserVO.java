package com.zq.qingzhu.controller.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class UserVO implements Serializable {

	private Integer userId;

	private String userName;

	private String loginName;

	private String address;

	private int shopCartCount;

	private BigDecimal shopCartTotalMoney;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getShopCartCount() {
		return shopCartCount;
	}

	public void setShopCartCount(int shopCartCount) {
		this.shopCartCount = shopCartCount;
	}

	public BigDecimal getShopCartTotalMoney() {
		return shopCartTotalMoney;
	}

	public void setShopCartTotalMoney(BigDecimal shopCartTotalMoney) {
		this.shopCartTotalMoney = shopCartTotalMoney;
	}
}
