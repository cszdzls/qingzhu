package com.zq.qingzhu.controller.vo;

import java.io.Serializable;

/**
 * 首页分类数据VO(第三级)
 */
public class ThirdLevelCategoryVO implements Serializable {

	private Integer categoryId;

	private Byte categoryLevel;

	private String categoryName;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Byte getCategoryLevel() {
		return categoryLevel;
	}

	public void setCategoryLevel(Byte categoryLevel) {
		this.categoryLevel = categoryLevel;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
