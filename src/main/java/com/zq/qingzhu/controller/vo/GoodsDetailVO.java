package com.zq.qingzhu.controller.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 商品详情页VO
 */
public class GoodsDetailVO implements Serializable {

	private Integer goodsId;

	private String goodsName;

	private String goodsDescription;

	private Integer stock;

	private String goodsCoverImg;

	private String goodsSmallImg;

	private String[] goodsCarouselList;

	private BigDecimal sellingPrice;

	public Integer getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsDescription() {
		return goodsDescription;
	}

	public void setGoodsDescription(String goodsDescription) {
		this.goodsDescription = goodsDescription;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String getGoodsCoverImg() {
		return goodsCoverImg;
	}

	public void setGoodsCoverImg(String goodsCoverImg) {
		this.goodsCoverImg = goodsCoverImg;
	}

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getGoodsSmallImg() {
		return goodsSmallImg;
	}

	public void setGoodsSmallImg(String goodsSmallImg) {
		this.goodsSmallImg = goodsSmallImg;
	}

	public String[] getGoodsCarouselList() {
		return goodsCarouselList;
	}

	public void setGoodsCarouselList(String[] goodsCarouselList) {
		this.goodsCarouselList = goodsCarouselList;
	}
}
