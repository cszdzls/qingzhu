package com.zq.qingzhu.config;

import com.zq.qingzhu.common.Constants;
import com.zq.qingzhu.interceptor.AdminLoginInterceptor;
import com.zq.qingzhu.interceptor.CartNumberInterceptor;
import com.zq.qingzhu.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfigurer implements WebMvcConfigurer {

	@Autowired
	private AdminLoginInterceptor adminLoginInterceptor;
	@Autowired
	private LoginInterceptor loginInterceptor;
	@Autowired
	private CartNumberInterceptor cartNumberInterceptor;

	public void addInterceptors(InterceptorRegistry registry) {
		// 添加一个拦截器，拦截以/admin为前缀的url路径（后台登陆拦截）
		registry.addInterceptor(adminLoginInterceptor)
				.addPathPatterns("/admin/**")
				.excludePathPatterns("/admin/login")
				.excludePathPatterns("/admin/dist/**")
				.excludePathPatterns("/admin/plugins/**");
		// 购物车中的数量统一处理
		registry.addInterceptor(cartNumberInterceptor)
				.excludePathPatterns("/admin/**")
				.excludePathPatterns("/register")
				.excludePathPatterns("/login")
				.excludePathPatterns("/logout");
		// 商城页面登陆拦截
		registry.addInterceptor(loginInterceptor)
				.excludePathPatterns("/admin/**")
				.excludePathPatterns("/register")
				.excludePathPatterns("/login")
				.excludePathPatterns("/logout")
				.excludePathPatterns("/goods/detail/**")
				.addPathPatterns("/shop-cart")
				.addPathPatterns("/shop-cart/**")
				.addPathPatterns("/shopcart.html")
				.addPathPatterns("/saveOrder")
				.addPathPatterns("/orders")
				.addPathPatterns("/orders/**")
				.addPathPatterns("/personal")
				.addPathPatterns("/userinfo.html")
				.addPathPatterns("/personal/updateInfo")
				.addPathPatterns("/selectPayType")
				.addPathPatterns("/payPage");
	}

	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/upload/**").addResourceLocations("file:" + Constants.FILE_UPLOAD_DIC);
		registry.addResourceHandler("/goods-img/**").addResourceLocations("file:" + Constants.FILE_UPLOAD_DIC);
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
//		registry.addViewController("/shopcart.html").setViewName("qingzhu/shopcart");
	}
}
