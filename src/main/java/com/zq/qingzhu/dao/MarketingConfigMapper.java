package com.zq.qingzhu.dao;

import com.zq.qingzhu.bean.MarketingConfig;
import com.zq.qingzhu.bean.MarketingConfigExample;
import com.zq.qingzhu.util.PageQueryUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MarketingConfigMapper {
	int countByExample(MarketingConfigExample example);

	int deleteByExample(MarketingConfigExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(MarketingConfig record);

	int insertSelective(MarketingConfig record);

	List<MarketingConfig> selectByExample(MarketingConfigExample example);

	MarketingConfig selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") MarketingConfig record, @Param("example") MarketingConfigExample example);

	int updateByExample(@Param("record") MarketingConfig record, @Param("example") MarketingConfigExample example);

	int updateByPrimaryKeySelective(MarketingConfig record);

	int updateByPrimaryKey(MarketingConfig record);

	List<MarketingConfig> findMarketingConfigList(PageQueryUtil pageUtil);

	int getTotalMarketingConfigs(PageQueryUtil pageUtil);

	int deleteBatch(Integer[] ids);

	List<MarketingConfig> findMarketingConfigsByTypeAndNum(@Param("configType") int configType, @Param("number") int number);
}