package com.zq.qingzhu.dao;

import com.zq.qingzhu.bean.Category;
import com.zq.qingzhu.bean.CategoryExample;
import com.zq.qingzhu.util.PageQueryUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategoryMapper {
	int countByExample(CategoryExample example);

	int deleteByExample(CategoryExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(Category record);

	int insertSelective(Category record);

	List<Category> selectByExample(CategoryExample example);

	Category selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") Category record, @Param("example") CategoryExample example);

	int updateByExample(@Param("record") Category record, @Param("example") CategoryExample example);

	int updateByPrimaryKeySelective(Category record);

	int updateByPrimaryKey(Category record);

	List<Category> findGoodsCategoryList(PageQueryUtil pageUtil);

	int getTotalGoodsCategories(PageQueryUtil pageUtil);

	int deleteBatch(Integer[] ids);

	@Deprecated
	List<Category> selectByLevelAndParentIdsAndNumber(@Param("parentIds") List<Integer> parentIds, @Param("categoryLevel") int categoryLevel, @Param("number") int number);

	@Deprecated
	Category selectByLevelAndName(@Param("categoryLevel") Byte categoryLevel, @Param("categoryName") String categoryName);

	List<Category> getAllCategory();
}