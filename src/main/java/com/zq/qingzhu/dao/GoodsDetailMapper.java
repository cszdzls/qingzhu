package com.zq.qingzhu.dao;

import com.zq.qingzhu.bean.GoodsDetail;
import com.zq.qingzhu.bean.GoodsDetailExample;
import com.zq.qingzhu.bean.StockNumDTO;
import com.zq.qingzhu.util.PageQueryUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GoodsDetailMapper {
	int countByExample(GoodsDetailExample example);

	int deleteByExample(GoodsDetailExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(GoodsDetail record);

	int insertSelective(GoodsDetail record);

	List<GoodsDetail> selectByExample(GoodsDetailExample example);

	GoodsDetail selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") GoodsDetail record, @Param("example") GoodsDetailExample example);

	int updateByExample(@Param("record") GoodsDetail record, @Param("example") GoodsDetailExample example);

	int updateByPrimaryKeySelective(GoodsDetail record);

	int updateByPrimaryKey(GoodsDetail record);

	List<GoodsDetail> selectByPrimaryKeys(List<Integer> goodsIds);

	List<GoodsDetail> findGoodsList(PageQueryUtil pageUtil);

	int getTotalGoods(PageQueryUtil pageUtil);

	List<GoodsDetail> findGoodsListBySearch(PageQueryUtil pageUtil);

	int getTotalGoodsBySearch(PageQueryUtil pageUtil);

	int batchInsert(@Param("goodsList") List<GoodsDetail> goodsList);

	int updateStockNum(@Param("stockNumDTOS") List<StockNumDTO> stockNumDTOS);

	int batchUpdateSellStatus(@Param("orderIds") Integer[] orderIds, @Param("sellStatus") int sellStatus);

	List<String> getAllBrands();
}