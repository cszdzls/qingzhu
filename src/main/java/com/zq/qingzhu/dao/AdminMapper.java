package com.zq.qingzhu.dao;

import com.zq.qingzhu.bean.Admin;
import com.zq.qingzhu.bean.AdminExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminMapper {
	int countByExample(AdminExample example);

	int deleteByExample(AdminExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(Admin record);

	int insertSelective(Admin record);

	List<Admin> selectByExample(AdminExample example);

	Admin selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") Admin record, @Param("example") AdminExample example);

	int updateByExample(@Param("record") Admin record, @Param("example") AdminExample example);

	int updateByPrimaryKeySelective(Admin record);

	int updateByPrimaryKey(Admin record);

	/**
	 * 登陆方法
	 *
	 * @param userName
	 * @param password
	 * @return
	 */
	Admin login(@Param("userName") String userName, @Param("password") String password);
}