package com.zq.qingzhu.dao;

import com.zq.qingzhu.bean.ShopCart;
import com.zq.qingzhu.bean.ShopCartExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShopCartMapper {
	int countByExample(ShopCartExample example);

	int deleteByExample(ShopCartExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(ShopCart record);

	int insertSelective(ShopCart record);

	List<ShopCart> selectByExample(ShopCartExample example);

	ShopCart selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") ShopCart record, @Param("example") ShopCartExample example);

	int updateByExample(@Param("record") ShopCart record, @Param("example") ShopCartExample example);

	int updateByPrimaryKeySelective(ShopCart record);

	int updateByPrimaryKey(ShopCart record);

	int selectCountByUserId(Integer userId);

	ShopCart selectByUserIdAndGoodsId(@Param("userId") Integer userId, @Param("goodsId") Integer goodsId);

	List<ShopCart> selectByUserId(@Param("userId") Integer userId, @Param("limit") int limit);

	int deleteBatch(List<Integer> ids);
}