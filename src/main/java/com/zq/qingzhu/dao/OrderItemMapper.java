package com.zq.qingzhu.dao;

import com.zq.qingzhu.bean.OrderItem;
import com.zq.qingzhu.bean.OrderItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderItemMapper {
	int countByExample(OrderItemExample example);

	int deleteByExample(OrderItemExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(OrderItem record);

	int insertSelective(OrderItem record);

	List<OrderItem> selectByExample(OrderItemExample example);

	OrderItem selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") OrderItem record, @Param("example") OrderItemExample example);

	int updateByExample(@Param("record") OrderItem record, @Param("example") OrderItemExample example);

	int updateByPrimaryKeySelective(OrderItem record);

	int updateByPrimaryKey(OrderItem record);

	/**
	 * 根据订单id获取订单项列表
	 *
	 * @param orderId
	 * @return
	 */
	List<OrderItem> selectByOrderId(Integer orderId);

	/**
	 * 根据订单ids获取订单项列表
	 *
	 * @param orderIds
	 * @return
	 */
	List<OrderItem> selectByOrderIds(@Param("orderIds") List<Integer> orderIds);

	/**
	 * 批量insert订单项数据
	 *
	 * @param orderItems
	 * @return
	 */
	int insertBatch(@Param("orderItems") List<OrderItem> orderItems);
}