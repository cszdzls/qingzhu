package com.zq.qingzhu.dao;

import com.zq.qingzhu.bean.OrderRecord;
import com.zq.qingzhu.bean.OrderRecordExample;
import com.zq.qingzhu.util.PageQueryUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderRecordMapper {
	int countByExample(OrderRecordExample example);

	int deleteByExample(OrderRecordExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(OrderRecord record);

	int insertSelective(OrderRecord record);

	List<OrderRecord> selectByExample(OrderRecordExample example);

	OrderRecord selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") OrderRecord record, @Param("example") OrderRecordExample example);

	int updateByExample(@Param("record") OrderRecord record, @Param("example") OrderRecordExample example);

	int updateByPrimaryKeySelective(OrderRecord record);

	int updateByPrimaryKey(OrderRecord record);

	OrderRecord selectByOrderNo(String orderNo);

	List<OrderRecord> selectByPrimaryKeys(@Param("orderIds") List<Integer> orderIds);

	List<OrderRecord> findOrderList(PageQueryUtil pageUtil);

	int getTotalOrders(PageQueryUtil pageUtil);

	int checkOut(@Param("orderIds") List<Integer> orderIds);

	int closeOrder(@Param("orderIds") List<Integer> orderIds, @Param("orderStatus") int orderStatus);

	int checkDone(@Param("orderIds") List<Integer> asList);
}