package com.zq.qingzhu.dao;

import com.zq.qingzhu.bean.Carousel;
import com.zq.qingzhu.bean.CarouselExample;
import com.zq.qingzhu.util.PageQueryUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CarouselMapper {
	int countByExample(CarouselExample example);

	int deleteByExample(CarouselExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(Carousel record);

	int insertSelective(Carousel record);

	List<Carousel> selectByExample(CarouselExample example);

	Carousel selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") Carousel record, @Param("example") CarouselExample example);

	int updateByExample(@Param("record") Carousel record, @Param("example") CarouselExample example);

	int updateByPrimaryKeySelective(Carousel record);

	int updateByPrimaryKey(Carousel record);

	List<Carousel> findCarouselList(PageQueryUtil pageUtil);

	int getTotalCarousels(PageQueryUtil pageUtil);

	int deleteBatch(Integer[] ids);

	List<Carousel> findCarouselsByNum(@Param("number") int number);
}