package com.zq.qingzhu.service;


import com.zq.qingzhu.bean.User;
import com.zq.qingzhu.controller.vo.UserVO;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.PageResult;

import javax.servlet.http.HttpSession;

public interface UserService {
	/**
	 * 后台分页
	 *
	 * @param pageUtil
	 * @return
	 */
	PageResult getUsersPage(PageQueryUtil pageUtil);

	/**
	 * 用户注册
	 *
	 * @param loginName
	 * @param password
	 * @return
	 */
	String register(String loginName, String password);

	/**
	 * 登录
	 *
	 * @param loginName
	 * @param password512
	 * @param httpSession
	 * @return
	 */
	String login(String loginName, String password512, HttpSession httpSession);

	/**
	 * 用户信息修改并返回最新的用户信息
	 *
	 * @param user
	 * @param httpSession
	 * @return
	 */
	UserVO updateUserInfo(User user, HttpSession httpSession);

	/**
	 * 用户禁用与解除禁用(0-未锁定 1-已锁定)
	 *
	 * @param ids
	 * @param lockStatus
	 * @return
	 */
	Boolean lockUsers(Integer[] ids, int lockStatus);
}
