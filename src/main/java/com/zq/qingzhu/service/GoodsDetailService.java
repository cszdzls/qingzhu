package com.zq.qingzhu.service;

import com.zq.qingzhu.bean.GoodsDetail;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.PageResult;

import java.util.List;

public interface GoodsDetailService {
	/**
	 * 后台分页
	 *
	 * @param pageUtil
	 * @return
	 */
	PageResult getGoodsPage(PageQueryUtil pageUtil);

	/**
	 * 添加商品
	 *
	 * @param goods
	 * @return
	 */
	String saveGoods(GoodsDetail goods);

	/**
	 * 批量新增商品数据
	 *
	 * @param goodsList
	 * @return
	 */
	void batchSaveGoods(List<GoodsDetail> goodsList);

	/**
	 * 修改商品信息
	 *
	 * @param goods
	 * @return
	 */
	String updateGoods(GoodsDetail goods);

	/**
	 * 获取商品详情
	 *
	 * @param id
	 * @return
	 */
	GoodsDetail getGoodsById(Integer id);

	/**
	 * 批量修改销售状态(上架下架)
	 *
	 * @param ids
	 * @return
	 */
	Boolean batchUpdateSellStatus(Integer[] ids, int sellStatus);

	/**
	 * 商品搜索
	 *
	 * @param pageUtil
	 * @return
	 */
	PageResult searchGoods(PageQueryUtil pageUtil);

	/**
	 * 获得所有的商品品牌
	 * @return brandList
	 */
	List<String> getBrandList();

	/**
	 * 获得商品总数
	 * @return goodsCount
	 */
	Integer getGoodsCount();
}
