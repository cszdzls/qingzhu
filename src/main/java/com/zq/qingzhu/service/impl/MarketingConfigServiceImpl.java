package com.zq.qingzhu.service.impl;

import com.zq.qingzhu.bean.GoodsDetail;
import com.zq.qingzhu.bean.MarketingConfig;
import com.zq.qingzhu.common.ServiceResultEnum;
import com.zq.qingzhu.controller.vo.IndexConfigGoodsVO;
import com.zq.qingzhu.dao.GoodsDetailMapper;
import com.zq.qingzhu.dao.MarketingConfigMapper;
import com.zq.qingzhu.service.MarketingConfigService;
import com.zq.qingzhu.util.BeanUtil;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MarketingConfigServiceImpl implements MarketingConfigService {

	@Autowired
	private MarketingConfigMapper marketingConfigMapper;

	@Autowired
	private GoodsDetailMapper goodsDetailMapper;

	@Override
	public PageResult getConfigsPage(PageQueryUtil pageUtil) {
		List<MarketingConfig> indexConfigs = marketingConfigMapper.findMarketingConfigList(pageUtil);
		int total = marketingConfigMapper.getTotalMarketingConfigs(pageUtil);
		PageResult pageResult = new PageResult(indexConfigs, total, pageUtil.getLimit(), pageUtil.getPage());
		return pageResult;
	}

	@Override
	public String saveMarketingConfig(MarketingConfig indexConfig) {
		//todo 判断是否存在该商品
		if (marketingConfigMapper.insertSelective(indexConfig) > 0) {
			return ServiceResultEnum.SUCCESS.getResult();
		}
		return ServiceResultEnum.DB_ERROR.getResult();
	}

	@Override
	public String updateMarketingConfig(MarketingConfig indexConfig) {
		//todo 判断是否存在该商品
		MarketingConfig temp = marketingConfigMapper.selectByPrimaryKey(indexConfig.getId());
		if (temp == null) {
			return ServiceResultEnum.DATA_NOT_EXIST.getResult();
		}
		if (marketingConfigMapper.updateByPrimaryKeySelective(indexConfig) > 0) {
			return ServiceResultEnum.SUCCESS.getResult();
		}
		return ServiceResultEnum.DB_ERROR.getResult();
	}

	@Override
	public MarketingConfig getMarketingConfigById(Integer id) {
		return null;
	}

	@Override
	public List<IndexConfigGoodsVO> getConfigGoodsesForIndex(int configType, int number) {
		List<IndexConfigGoodsVO> indexConfigGoodsVOS = new ArrayList<>(number);
		List<MarketingConfig> indexConfigs = marketingConfigMapper.findMarketingConfigsByTypeAndNum(configType, number);
		if (!CollectionUtils.isEmpty(indexConfigs)) {
			//取出所有的goodsId
			List<Integer> goodsIds = indexConfigs.stream().map(MarketingConfig::getGoodsId).collect(Collectors.toList());
			List<GoodsDetail> goods = goodsDetailMapper.selectByPrimaryKeys(goodsIds);
			indexConfigGoodsVOS = BeanUtil.copyList(goods, IndexConfigGoodsVO.class);
			//IndexConfigGoodsVO的goodsId属性需要额外拷贝
			for (int i=0;i<indexConfigGoodsVOS.size();i++){
				indexConfigGoodsVOS.get(i).setGoodsId(goods.get(i).getId());
			}
			// 字符串过长导致文字超出的问题
			for (IndexConfigGoodsVO indexConfigGoodsVO : indexConfigGoodsVOS) {
				String goodsName = indexConfigGoodsVO.getGoodsName();
				String goodsIntro = indexConfigGoodsVO.getDescription();

				if (goodsName.length() > 30) {
					goodsName = goodsName.substring(0, 30) + "...";
					indexConfigGoodsVO.setGoodsName(goodsName);
				}
				if (goodsIntro.length() > 22) {
					goodsIntro = goodsIntro.substring(0, 22) + "...";
					indexConfigGoodsVO.setDescription(goodsIntro);
				}
			}
		}
		return indexConfigGoodsVOS;
	}

	@Override
	public Boolean deleteBatch(Integer[] ids) {
		if (ids.length < 1) {
			return false;
		}
		//删除数据
		return marketingConfigMapper.deleteBatch(ids) > 0;
	}
}
