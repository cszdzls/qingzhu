package com.zq.qingzhu.service.impl;

import com.zq.qingzhu.bean.GoodsDetail;
import com.zq.qingzhu.common.ServiceResultEnum;
import com.zq.qingzhu.controller.vo.SearchGoodsVO;
import com.zq.qingzhu.dao.GoodsDetailMapper;
import com.zq.qingzhu.service.GoodsDetailService;
import com.zq.qingzhu.util.BeanUtil;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class GoodsDetailServiceImpl implements GoodsDetailService {

	@Autowired
	private GoodsDetailMapper goodsMapper;

	@Override
	public PageResult getGoodsPage(PageQueryUtil pageUtil) {
		List<GoodsDetail> goodsList = goodsMapper.findGoodsList(pageUtil);
		int total = goodsMapper.getTotalGoods(pageUtil);
		PageResult pageResult = new PageResult(goodsList, total, pageUtil.getLimit(), pageUtil.getPage());
		return pageResult;
	}

	@Override
	public String saveGoods(GoodsDetail goods) {
		if (goodsMapper.insertSelective(goods) > 0) {
			return ServiceResultEnum.SUCCESS.getResult();
		}
		return ServiceResultEnum.DB_ERROR.getResult();
	}

	@Override
	public void batchSaveGoods(List<GoodsDetail> goodsList) {
		if (!CollectionUtils.isEmpty(goodsList)) {
			goodsMapper.batchInsert(goodsList);
		}
	}

	@Override
	public String updateGoods(GoodsDetail goods) {
		GoodsDetail temp = goodsMapper.selectByPrimaryKey(goods.getId());
		if (temp == null) {
			return ServiceResultEnum.DATA_NOT_EXIST.getResult();
		}
		goods.setUpdateTime(new Date());
		if (goodsMapper.updateByPrimaryKeySelective(goods) > 0) {
			return ServiceResultEnum.SUCCESS.getResult();
		}
		return ServiceResultEnum.DB_ERROR.getResult();
	}

	@Override
	public GoodsDetail getGoodsById(Integer id) {
		return goodsMapper.selectByPrimaryKey(id);
	}

	@Override
	public Boolean batchUpdateSellStatus(Integer[] ids, int sellStatus) {
		return goodsMapper.batchUpdateSellStatus(ids, sellStatus) > 0;
	}

	@Override
	public PageResult searchGoods(PageQueryUtil pageUtil) {
		List<GoodsDetail> goodsList = goodsMapper.findGoodsListBySearch(pageUtil);
		int total = goodsMapper.getTotalGoodsBySearch(pageUtil);
		List<SearchGoodsVO> searchGoodsVOS = new ArrayList<>();
		if (!CollectionUtils.isEmpty(goodsList)) {
			searchGoodsVOS = BeanUtil.copyList(goodsList, SearchGoodsVO.class);
			//额外拷贝goodsId, goodsIntro属性
			for (int i=0;i<goodsList.size();i++){
				searchGoodsVOS.get(i).setGoodsId(goodsList.get(i).getId());
				searchGoodsVOS.get(i).setGoodsIntro(goodsList.get(i).getDescription());
			}
			for (SearchGoodsVO searchGoodsVO : searchGoodsVOS) {
				String goodsName = searchGoodsVO.getGoodsName();
				String goodsIntro = searchGoodsVO.getGoodsIntro();
				// 字符串过长导致文字超出的问题
				if (goodsName.length() > 28) {
					goodsName = goodsName.substring(0, 28) + "...";
					searchGoodsVO.setGoodsName(goodsName);
				}
				if (goodsIntro.length() > 30) {
					goodsIntro = goodsIntro.substring(0, 30) + "...";
					searchGoodsVO.setGoodsIntro(goodsIntro);
				}
			}
		}
		return new PageResult(searchGoodsVOS, total, pageUtil.getLimit(), pageUtil.getPage());
	}

	@Override
	public List<String> getBrandList() {
		return goodsMapper.getAllBrands();
	}

	@Override
	public Integer getGoodsCount() {
		return goodsMapper.countByExample(null);
	}
}
