package com.zq.qingzhu.service.impl;

import com.zq.qingzhu.bean.Admin;
import com.zq.qingzhu.dao.AdminMapper;
import com.zq.qingzhu.service.AdminService;
import com.zq.qingzhu.util.SHA512;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AdminServiceImpl implements AdminService {

	@Resource
	private AdminMapper AdminMapper;

	@Override
	public Admin login(String userName, String password) {
		String password512 = SHA512.getSHA512(password);
		return AdminMapper.login(userName, password512);
	}

	@Override
	public Admin getUserDetailById(Integer loginUserId) {
		return AdminMapper.selectByPrimaryKey(loginUserId);
	}

	@Override
	public Boolean updatePassword(Integer loginUserId, String originalPassword, String newPassword) {
		Admin admin = AdminMapper.selectByPrimaryKey(loginUserId);
		//当前用户非空才可以进行更改
		if (admin != null) {
			String originalPassword512 = SHA512.getSHA512(originalPassword);
			String newPassword512 = SHA512.getSHA512(newPassword);
			//比较原密码是否正确
			if (originalPassword512.equals(admin.getPassword())) {
				//设置新密码并修改
				admin.setPassword(newPassword512);
				if (AdminMapper.updateByPrimaryKeySelective(admin) > 0) {
					//修改成功则返回true
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public Boolean updateName(Integer loginUserId, String loginUserName, String nickName) {
		Admin admin = AdminMapper.selectByPrimaryKey(loginUserId);
		//当前用户非空才可以进行更改
		if (admin != null) {
			//设置新名称并修改
			admin.setUsername(loginUserName);
			if (AdminMapper.updateByPrimaryKeySelective(admin) > 0) {
				//修改成功则返回true
				return true;
			}
		}
		return false;
	}
}
