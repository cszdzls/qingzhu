package com.zq.qingzhu.service.impl;

import com.zq.qingzhu.bean.GoodsDetail;
import com.zq.qingzhu.bean.ShopCart;
import com.zq.qingzhu.bean.ShopCartExample;
import com.zq.qingzhu.bean.User;
import com.zq.qingzhu.common.Constants;
import com.zq.qingzhu.common.ServiceResultEnum;
import com.zq.qingzhu.controller.vo.UserVO;
import com.zq.qingzhu.dao.GoodsDetailMapper;
import com.zq.qingzhu.dao.ShopCartMapper;
import com.zq.qingzhu.dao.UserMapper;
import com.zq.qingzhu.service.UserService;
import com.zq.qingzhu.util.BeanUtil;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.PageResult;
import com.zq.qingzhu.util.SHA512;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private ShopCartMapper shopCartMapper;
	@Autowired
	private GoodsDetailMapper goodsDetailMapper;

	@Override
	public PageResult getUsersPage(PageQueryUtil pageUtil) {
		List<User> users = userMapper.findMallUserList(pageUtil);
		int total = userMapper.getTotalUsers(pageUtil);
		PageResult pageResult = new PageResult(users, total, pageUtil.getLimit(), pageUtil.getPage());
		return pageResult;
	}

	@Override
	public String register(String loginName, String password) {
		if (userMapper.selectByLoginName(loginName) != null) {
			return ServiceResultEnum.SAME_LOGIN_NAME_EXIST.getResult();
		}
		User registerUser = new User();
		registerUser.setUsername(loginName);
		registerUser.setPhone(loginName);
//		registerUser.setNickName(loginName);
		String password512 = SHA512.getSHA512(password);
		registerUser.setPassword(password512);
		if (userMapper.insertSelective(registerUser) > 0) {
			return ServiceResultEnum.SUCCESS.getResult();
		}
		return ServiceResultEnum.DB_ERROR.getResult();
	}

	@Override
	public String login(String loginName, String password512, HttpSession httpSession) {
		User user = userMapper.selectByLoginNameAndPasswd(loginName, password512);
		if (user != null && httpSession != null) {
			if (user.getLockedFlag() == 1) {
				return ServiceResultEnum.LOGIN_USER_LOCKED.getResult();
			}
			//昵称太长 影响页面展示
			if (user.getUsername() != null && user.getUsername().length() > 7) {
				String tempNickName = user.getUsername().substring(0, 7) + "..";
				user.setUsername(tempNickName);
			}
			UserVO userVO = new UserVO();
//			BeanUtil.copyProperties(user, userVO);
			userVO.setUserId(user.getId());
			userVO.setUserName(user.getUsername());
			userVO.setLoginName(user.getPhone());
			userVO.setAddress(user.getAddress());

			//获得购物车中商品的数量
			ShopCartExample example = new ShopCartExample();
			example.createCriteria().andUserIdEqualTo(user.getId());
			List<ShopCart> shopCartList = shopCartMapper.selectByExample(example);
			//获得购物车中所有的商品id
			List<Integer> goodsIds = new ArrayList<>();
			for (ShopCart shopCart:shopCartList){
				goodsIds.add(shopCart.getGoodsId());
			}
			//获得购物车总价和商品总数
			List<GoodsDetail> goodsDetailList = goodsIds.size()==0?new ArrayList<>():goodsDetailMapper.selectByPrimaryKeys(goodsIds);
			int totalCount = 0;
			BigDecimal totalPrice = new BigDecimal("0.00");
			for (int i=0;i<shopCartList.size();i++){
				ShopCart shopCart = shopCartList.get(i);
				GoodsDetail goodsDetail = goodsDetailList.get(i);
				totalCount += shopCart.getGoodsCount();
				totalPrice = totalPrice.add(goodsDetail.getSellingPrice().multiply(BigDecimal.valueOf(shopCart.getGoodsCount())));
			}
			userVO.setShopCartCount(totalCount);
			userVO.setShopCartTotalMoney(totalPrice);
			//设置购物车中的数量
			httpSession.setAttribute(Constants.MALL_USER_SESSION_KEY, userVO);
			return ServiceResultEnum.SUCCESS.getResult();
		}
		return ServiceResultEnum.LOGIN_ERROR.getResult();
	}

	@Override
	public UserVO updateUserInfo(User user, HttpSession httpSession) {
		User newUser = userMapper.selectByPrimaryKey(user.getId());
		if (newUser!=null) {
			newUser.setUsername(user.getUsername());
			newUser.setAddress(user.getAddress());
//			user.setIntroduceSign(user.getIntroduceSign());
			if (userMapper.updateByPrimaryKeySelective(newUser) > 0) {
				UserVO userVO = new UserVO();
				newUser = userMapper.selectByPrimaryKey(user.getId());
				BeanUtil.copyProperties(newUser, userVO);
				httpSession.setAttribute(Constants.MALL_USER_SESSION_KEY, userVO);
				return userVO;
			}
		}
		return null;
	}

	@Override
	public Boolean lockUsers(Integer[] ids, int lockStatus) {
		if (ids.length < 1) {
			return false;
		}
		return userMapper.lockUserBatch(ids, lockStatus) > 0;
	}
}
