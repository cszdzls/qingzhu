package com.zq.qingzhu.service.impl;

import com.zq.qingzhu.bean.GoodsDetail;
import com.zq.qingzhu.bean.ShopCart;
import com.zq.qingzhu.common.Constants;
import com.zq.qingzhu.common.ServiceResultEnum;
import com.zq.qingzhu.controller.vo.ShoppingCartItemVO;
import com.zq.qingzhu.dao.GoodsDetailMapper;
import com.zq.qingzhu.dao.ShopCartMapper;
import com.zq.qingzhu.service.ShopCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ShopCartServiceImpl implements ShopCartService {

	@Autowired
	private ShopCartMapper shopCartMapper;

	@Autowired
	private GoodsDetailMapper goodsDetailMapper;

	//todo 修改session中购物项数量

	@Override
	public String saveShopCart(ShopCart shopCart) {
		ShopCart temp = shopCartMapper.selectByUserIdAndGoodsId(shopCart.getUserId(), shopCart.getGoodsId());
		if (temp != null) {
			//已存在则修改该记录
			//todo count = tempCount + 1
			temp.setGoodsCount(shopCart.getGoodsCount());
			return updateShopCart(temp);
		}
		//用户或商品不存在，接下来确认具体问题
		GoodsDetail goods = goodsDetailMapper.selectByPrimaryKey(shopCart.getGoodsId());
		//商品为空
		if (goods == null) {
			return ServiceResultEnum.GOODS_NOT_EXIST.getResult();
		}
		int totalItem = shopCartMapper.selectCountByUserId(shopCart.getUserId());
		//超出最大数量
		if (totalItem > Constants.SHOPPING_CART_ITEM_LIMIT_NUMBER) {
			return ServiceResultEnum.SHOPPING_CART_ITEM_LIMIT_NUMBER_ERROR.getResult();
		}
		//保存记录
		if (shopCartMapper.insertSelective(shopCart) > 0) {
			return ServiceResultEnum.SUCCESS.getResult();
		}
		return ServiceResultEnum.DB_ERROR.getResult();
	}

	@Override
	public String updateShopCart(ShopCart shopCart) {
		ShopCart shoppingCartItemUpdate = shopCartMapper.selectByPrimaryKey(shopCart.getId());
		if (shoppingCartItemUpdate == null) {
			return ServiceResultEnum.DATA_NOT_EXIST.getResult();
		}
		//超出最大数量
		if (shopCart.getGoodsCount() > Constants.SHOPPING_CART_ITEM_LIMIT_NUMBER) {
			return ServiceResultEnum.SHOPPING_CART_ITEM_LIMIT_NUMBER_ERROR.getResult();
		}
		//todo 数量相同不会进行修改
		//todo userId不同不能修改
		shoppingCartItemUpdate.setGoodsCount(shopCart.getGoodsCount());
		shoppingCartItemUpdate.setUpdateTime(new Date());
		//保存记录
		if (shopCartMapper.updateByPrimaryKeySelective(shoppingCartItemUpdate) > 0) {
			return ServiceResultEnum.SUCCESS.getResult();
		}
		return ServiceResultEnum.DB_ERROR.getResult();
	}

	@Override
	public ShopCart getShopCartById(Integer shopCartId) {
		return shopCartMapper.selectByPrimaryKey(shopCartId);
	}

	@Override
	public Boolean deleteById(Integer shopCartId) {
		//todo userId不同不能删除
		return shopCartMapper.deleteByPrimaryKey(shopCartId) > 0;
	}

	@Override
	public List<ShoppingCartItemVO> getMyShoppingCartItems(Integer userId) {
		List<ShoppingCartItemVO> shoppingCartItemVOS = new ArrayList<>();
		List<ShopCart> shopCarts = shopCartMapper.selectByUserId(userId, Constants.SHOPPING_CART_ITEM_TOTAL_NUMBER);
		if (!CollectionUtils.isEmpty(shopCarts)) {
			//查询商品信息并做数据转换
			List<Integer> goodsIds = shopCarts.stream().map(ShopCart::getGoodsId).collect(Collectors.toList());
			List<GoodsDetail> goods = goodsDetailMapper.selectByPrimaryKeys(goodsIds);
			Map<Integer, GoodsDetail> goodsMap = new HashMap<>();
			if (!CollectionUtils.isEmpty(goods)) {
				goodsMap = goods.stream().collect(Collectors.toMap(GoodsDetail::getId, Function.identity(), (entity1, entity2) -> entity1));
			}
			for (ShopCart shopCart : shopCarts) {
				ShoppingCartItemVO shoppingCartItemVO = new ShoppingCartItemVO();
//				BeanUtil.copyProperties(shopCart, shoppingCartItemVO);
				//填充VO类的属性
				shoppingCartItemVO.setCartItemId(shopCart.getId());
				shoppingCartItemVO.setGoodsId(shopCart.getGoodsId());
				shoppingCartItemVO.setGoodsCount(shopCart.getGoodsCount());

				if (goodsMap.containsKey(shopCart.getGoodsId())) {
					GoodsDetail goodsTemp = goodsMap.get(shopCart.getGoodsId());
					shoppingCartItemVO.setGoodsCoverImg(goodsTemp.getGoodsCoverImg());
					String goodsName = goodsTemp.getGoodsName();
					// 字符串过长导致文字超出的问题
					if (goodsName.length() > 28) {
						goodsName = goodsName.substring(0, 28) + "...";
					}
					shoppingCartItemVO.setGoodsName(goodsName);
					shoppingCartItemVO.setSellingPrice(goodsTemp.getSellingPrice());
					shoppingCartItemVOS.add(shoppingCartItemVO);
				}
			}
		}
		return shoppingCartItemVOS;
	}
}
