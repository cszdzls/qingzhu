package com.zq.qingzhu.service.impl;

import com.zq.qingzhu.bean.Category;
import com.zq.qingzhu.bean.CategoryExample;
import com.zq.qingzhu.common.ServiceResultEnum;
import com.zq.qingzhu.controller.vo.IndexCategoryVO;
import com.zq.qingzhu.controller.vo.SearchPageCategoryVO;
import com.zq.qingzhu.dao.CategoryMapper;
import com.zq.qingzhu.service.CategoryService;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryMapper categoryMapper;

	@Override
	public PageResult getCategorisPage(PageQueryUtil pageUtil) {
		List<Category> goodsCategories = categoryMapper.findGoodsCategoryList(pageUtil);
		int total = categoryMapper.getTotalGoodsCategories(pageUtil);
		PageResult pageResult = new PageResult(goodsCategories, total, pageUtil.getLimit(), pageUtil.getPage());
		return pageResult;
	}

	@Override
	public String saveCategory(Category goodsCategory) {
//		Category temp = categoryMapper.selectByLevelAndName(goodsCategory.getCategoryLevel(), goodsCategory.getCategoryName());
//		if (temp != null) {
//			return ServiceResultEnum.SAME_CATEGORY_EXIST.getResult();
//		}
//		if (categoryMapper.insertSelective(goodsCategory) > 0) {
//			return ServiceResultEnum.SUCCESS.getResult();
//		}
//		return ServiceResultEnum.DB_ERROR.getResult();
		CategoryExample categoryExample = new CategoryExample();
		categoryExample.createCriteria().andCategoryNameEqualTo(goodsCategory.getCategoryName());
		Category temp = categoryMapper.selectByExample(categoryExample).get(0);
		if(temp!=null){
			return ServiceResultEnum.SAME_CATEGORY_EXIST.getResult();
		}
	//	CategoryExample example = new CategoryExample();
	//	example.createCriteria().andCategoryNameEqualTo(goodsCategory.getCategoryName());
		if(categoryMapper.insertSelective(goodsCategory)>0){
			return ServiceResultEnum.SUCCESS.getResult();
		}
		return ServiceResultEnum.DB_ERROR.getResult();
	}

	@Override
	public String updateCategory(Category goodsCategory) {
//		Category temp = categoryMapper.selectByPrimaryKey(goodsCategory.getId());
//		if (temp == null) {
//			return ServiceResultEnum.DATA_NOT_EXIST.getResult();
//		}
//		Category temp2 = categoryMapper.selectByLevelAndName(goodsCategory.getCategoryLevel(), goodsCategory.getCategoryName());
//		if (temp2 != null && !temp2.getCategoryId().equals(goodsCategory.getCategoryId())) {
//			//同名且不同id 不能继续修改
//			return ServiceResultEnum.SAME_CATEGORY_EXIST.getResult();
//		}
//		goodsCategory.setUpdateTime(new Date());
//		if (categoryMapper.updateByPrimaryKeySelective(goodsCategory) > 0) {
//			return ServiceResultEnum.SUCCESS.getResult();
//		}
//		return ServiceResultEnum.DB_ERROR.getResult();
		return null;
	}

	@Override
	public Category getCategoryById(Integer id) {
		return categoryMapper.selectByPrimaryKey(id);
	}

	@Override
	public Boolean deleteBatch(Integer[] ids) {
		if (ids.length < 1) {
			return false;
		}
		//删除分类数据
		return categoryMapper.deleteBatch(ids) > 0;
	}

	@Override
	public List<IndexCategoryVO> getCategoriesForIndex() {
//		List<IndexCategoryVO> indexCategoryVOS = new ArrayList<>();
//		//获取一级分类的固定数量的数据
//		List<Category> firstLevelCategories = categoryMapper.selectByLevelAndParentIdsAndNumber(Collections.singletonList(0L), CategoryLevelEnum.LEVEL_ONE.getLevel(), Constants.INDEX_CATEGORY_NUMBER);
//		if (!CollectionUtils.isEmpty(firstLevelCategories)) {
//			List<Integer> firstLevelCategoryIds = firstLevelCategories.stream().map(Category::getCategoryId).collect(Collectors.toList());
//			//获取二级分类的数据
//			List<Category> secondLevelCategories = categoryMapper.selectByLevelAndParentIdsAndNumber(firstLevelCategoryIds, CategoryLevelEnum.LEVEL_TWO.getLevel(), 0);
//			if (!CollectionUtils.isEmpty(secondLevelCategories)) {
//				List<Integer> secondLevelCategoryIds = secondLevelCategories.stream().map(Category::getCategoryId).collect(Collectors.toList());
//				//获取三级分类的数据
//				List<Category> thirdLevelCategories = categoryMapper.selectByLevelAndParentIdsAndNumber(secondLevelCategoryIds, CategoryLevelEnum.LEVEL_THREE.getLevel(), 0);
//				if (!CollectionUtils.isEmpty(thirdLevelCategories)) {
//					//根据 parentId 将 thirdLevelCategories 分组
//					Map<Integer, List<Category>> thirdLevelCategoryMap = thirdLevelCategories.stream().collect(groupingBy(Category::getParentId));
//					List<SecondLevelCategoryVO> secondLevelCategoryVOS = new ArrayList<>();
//					//处理二级分类
//					for (Category secondLevelCategory : secondLevelCategories) {
//						SecondLevelCategoryVO secondLevelCategoryVO = new SecondLevelCategoryVO();
//						BeanUtil.copyProperties(secondLevelCategory, secondLevelCategoryVO);
//						//如果该二级分类下有数据则放入 secondLevelCategoryVOS 对象中
//						if (thirdLevelCategoryMap.containsKey(secondLevelCategory.getId())) {
//							//根据二级分类的id取出thirdLevelCategoryMap分组中的三级分类list
//							List<Category> tempGoodsCategories = thirdLevelCategoryMap.get(secondLevelCategory.getCategoryId());
//							secondLevelCategoryVO.setThirdLevelCategoryVOS((BeanUtil.copyList(tempGoodsCategories, ThirdLevelCategoryVO.class)));
//							secondLevelCategoryVOS.add(secondLevelCategoryVO);
//						}
//					}
//					//处理一级分类
//					if (!CollectionUtils.isEmpty(secondLevelCategoryVOS)) {
//						//根据 parentId 将 thirdLevelCategories 分组
//						Map<Integer, List<SecondLevelCategoryVO>> secondLevelCategoryVOMap = secondLevelCategoryVOS.stream().collect(groupingBy(SecondLevelCategoryVO::getParentId));
//						for (Category firstCategory : firstLevelCategories) {
//							IndexCategoryVO indexCategoryVO = new IndexCategoryVO();
//							BeanUtil.copyProperties(firstCategory, indexCategoryVO);
//							//如果该一级分类下有数据则放入 indexCategoryVOS 对象中
//							if (secondLevelCategoryVOMap.containsKey(firstCategory.getCategoryId())) {
//								//根据一级分类的id取出secondLevelCategoryVOMap分组中的二级级分类list
//								List<SecondLevelCategoryVO> tempGoodsCategories = secondLevelCategoryVOMap.get(firstCategory.getCategoryId());
//								indexCategoryVO.setSecondLevelCategoryVOS(tempGoodsCategories);
//								indexCategoryVOS.add(indexCategoryVO);
//							}
//						}
//					}
//				}
//			}
//			return indexCategoryVOS;
//		} else {
//			return null;
//		}
		List<IndexCategoryVO> indexCategoryVOS = new ArrayList<>();
		List<Category> categoryList = categoryMapper.selectByExample(null);
		for (Category category:categoryList){
			IndexCategoryVO vo = new IndexCategoryVO();
			vo.setCategoryId(category.getId());
			vo.setCategoryName(category.getCategoryName());
			indexCategoryVOS.add(vo);
		}
		return indexCategoryVOS;
	}

	@Override
	public SearchPageCategoryVO getCategoriesForSearch(Integer categoryId, String brandName) {

		return null;
	}

	@Override
	public List<Category> selectByLevelAndParentIdsAndNumber(List<Integer> parentIds, int categoryLevel) {
		return categoryMapper.selectByLevelAndParentIdsAndNumber(parentIds, categoryLevel, 0);//0代表查询所有
	}

	/**
	 * 返回所有商品类别列表
	 * @return allCategory
	 */
	@Override
	public List<Category> getAllCategory() {
		return categoryMapper.getAllCategory();
	}

	@Override
	public Integer getIdByCategoryName(String categoryName) {
		CategoryExample example = new CategoryExample();
		example.createCriteria().andCategoryNameEqualTo(categoryName);
		return categoryMapper.selectByExample(example).get(0).getId();
	}
}
