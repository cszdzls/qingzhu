package com.zq.qingzhu.service;


import com.zq.qingzhu.bean.OrderRecord;
import com.zq.qingzhu.controller.vo.OrderDetailVO;
import com.zq.qingzhu.controller.vo.OrderItemVO;
import com.zq.qingzhu.controller.vo.ShoppingCartItemVO;
import com.zq.qingzhu.controller.vo.UserVO;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.PageResult;

import java.util.List;

public interface OrderService {
	/**
	 * 后台分页
	 *
	 * @param pageUtil
	 * @return
	 */
	PageResult getOrdersPage(PageQueryUtil pageUtil);

	/**
	 * 订单信息修改
	 *
	 * @param order
	 * @return
	 */
	String updateOrderInfo(OrderRecord order);

	/**
	 * 配货
	 *
	 * @param ids
	 * @return
	 */
	String checkDone(Integer[] ids);

	/**
	 * 出库
	 *
	 * @param ids
	 * @return
	 */
	String checkOut(Integer[] ids);

	/**
	 * 关闭订单
	 *
	 * @param ids
	 * @return
	 */
	String closeOrder(Integer[] ids);

	/**
	 * 保存订单
	 *
	 * @param user
	 * @param myShoppingCartItems
	 * @return
	 */
	String saveOrder(UserVO user, List<ShoppingCartItemVO> myShoppingCartItems);

	/**
	 * 获取订单详情
	 *
	 * @param orderNo
	 * @param userId
	 * @return
	 */
	OrderDetailVO getOrderDetailByOrderNo(String orderNo, Integer userId);

	/**
	 * 获取订单详情
	 *
	 * @param orderNo
	 * @return
	 */
	OrderRecord getOrderByOrderNo(String orderNo);

	/**
	 * 我的订单列表
	 *
	 * @param pageUtil
	 * @return
	 */
	PageResult getMyOrders(PageQueryUtil pageUtil);

	/**
	 * 手动取消订单
	 *
	 * @param orderNo
	 * @param userId
	 * @return
	 */
	String cancelOrder(String orderNo, Integer userId);

	/**
	 * 确认收货
	 *
	 * @param orderNo
	 * @param userId
	 * @return
	 */
	String finishOrder(String orderNo, Integer userId);

	String paySuccess(String orderNo, int payType);

	List<OrderItemVO> getOrderItems(Integer id);

	List<OrderItemVO> getOrderItems(String orderNo);
}
