package com.zq.qingzhu.service;


import com.zq.qingzhu.bean.Category;
import com.zq.qingzhu.controller.vo.IndexCategoryVO;
import com.zq.qingzhu.controller.vo.SearchPageCategoryVO;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.PageResult;

import java.util.List;

public interface CategoryService {
	/**
	 * 后台分页
	 *
	 * @param pageUtil
	 * @return
	 */
	PageResult getCategorisPage(PageQueryUtil pageUtil);

	String saveCategory(Category goodsCategory);

	String updateCategory(Category goodsCategory);

	Category getCategoryById(Integer id);

	Boolean deleteBatch(Integer[] ids);

	/**
	 * 返回分类数据(首页调用)
	 *
	 * @return
	 */
	List<IndexCategoryVO> getCategoriesForIndex();

	/**
	 * 返回分类数据(搜索页调用)
	 *
	 * @param categoryId
	 * @return
	 */
	SearchPageCategoryVO getCategoriesForSearch(Integer categoryId, String brandName);

	/**
	 * 根据parentId和level获取分类列表
	 *
	 * @param parentIds
	 * @param categoryLevel
	 * @return
	 */
	List<Category> selectByLevelAndParentIdsAndNumber(List<Integer> parentIds, int categoryLevel);

	List<Category> getAllCategory();

	Integer getIdByCategoryName(String categoryName);
}
