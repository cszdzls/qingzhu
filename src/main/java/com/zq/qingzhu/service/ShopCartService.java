package com.zq.qingzhu.service;

import com.zq.qingzhu.bean.ShopCart;
import com.zq.qingzhu.controller.vo.ShoppingCartItemVO;

import java.util.List;

public interface ShopCartService {

	/**
	 * 保存商品至购物车中
	 *
	 * @param shopCart
	 * @return
	 */
	String saveShopCart(ShopCart shopCart);

	/**
	 * 修改购物车中的属性
	 *
	 * @param shopCart
	 * @return
	 */
	String updateShopCart(ShopCart shopCart);

	/**
	 * 获取购物项详情
	 *
	 * @param shopCartId
	 * @return
	 */
	ShopCart getShopCartById(Integer shopCartId);

	/**
	 * 删除购物车中的商品
	 *
	 * @param shopCartId
	 * @return
	 */
	Boolean deleteById(Integer shopCartId);

	/**
	 * 获取我的购物车中的列表数据
	 *
	 * @param userId
	 * @return
	 */
	List<ShoppingCartItemVO> getMyShoppingCartItems(Integer userId);
}
