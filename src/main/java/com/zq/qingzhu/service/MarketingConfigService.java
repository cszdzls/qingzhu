package com.zq.qingzhu.service;

import com.zq.qingzhu.bean.MarketingConfig;
import com.zq.qingzhu.controller.vo.IndexConfigGoodsVO;
import com.zq.qingzhu.util.PageQueryUtil;
import com.zq.qingzhu.util.PageResult;

import java.util.List;

public interface MarketingConfigService {
	/**
	 * 后台分页
	 *
	 * @param pageUtil
	 * @return
	 */
	PageResult getConfigsPage(PageQueryUtil pageUtil);

	String saveMarketingConfig(MarketingConfig marketingConfig);

	String updateMarketingConfig(MarketingConfig marketingConfig);

	MarketingConfig getMarketingConfigById(Integer id);

	/**
	 * 返回固定数量的首页配置商品对象(首页调用)
	 *
	 * @param number
	 * @return
	 */
	List<IndexConfigGoodsVO> getConfigGoodsesForIndex(int configType, int number);

	Boolean deleteBatch(Integer[] ids);
}
